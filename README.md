EmpoderemosMas.com
==================

Website built on Eleventy and Tailwindcss.
Initial project setup based on https://statickit.com/guides/eleventy-webpack and https://statickit.com/guides/eleventy-tailwind.

Template based on: https://github.com/tailwindtoolbox/Help-Article


## Serving the site Locally
```
# To build for production:
$ ELEVENTY_ENV=production yarn build

# To build and serve locally for development:
$ ELEVENTY_ENV=development yarn build && ELEVENTY_ENV=development yarn serve:eleventy

# To push content to Algolia index
yarn algolia:push
```


## Configuring CI/CD
Populate environments below:
- `WEB_ROOT_URL`   - Website's root URL, it is used to generate site.xml
- `GOOGLE_TAG_ID`  - Google Analytics tag ID 
- `ALGOLIA_APP_ID` - Algolia's APP ID 
- `ALGOLIA_ADMIN_API_kEY` - Algolia's Admin API Key (for pushing posts to Algolia index)

## Future Enhancements
- Add Google Analytics
- Comment (Commmento, Disqus, Facebook comment, or Custom with Firebase)
- Add `markdown` classname to markdown generated tags

## For new Eleventy + TailwindCSS project
```
yarn add --dev @11ty/eleventy moment @11ty/eleventy-plugin-syntaxhighlight
yarn add --dev markdown-it markdown-it-attrs
yarn add --dev webpack webpack-cli npm-run-all rimraf
yarn add --dev tailwindcss autoprefixer mini-css-extract-plugin css-loader postcss-loader
```

## Using fundamenty-cli
```sh
# Clean-up vtt (YoutTube subtitle), striping out metadata and leaving only text:
yarn fun-vtt staging/e05_ko-script-peligros-sociaedad-digital_ko.vtt -t "Dangers in digital society" -l ko
```

## Other useful articles
- Integrataion with Algolia: [link](https://www.raymondcamden.com/2020/06/24/adding-algolia-search-to-eleventy-and-netlify) and [link](https://www.raymondcamden.com/2020/07/01/adding-algolia-search-to-eleventy-and-netlify-part-two)
- Multilingual: [article](https://www.webstoemp.com/blog/multilingual-sites-eleventy/).

## Credits
- [Tailwind Help-Article Theme](https://github.com/tailwindtoolbox/Help-Article) from tailwindtoolbox
