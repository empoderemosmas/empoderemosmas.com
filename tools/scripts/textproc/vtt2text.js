const utils = require('../../utils');

const META = {
    name: 'textproc/vtt2text',
    dir: __dirname,
    description: "Strips out meta data from vtt file and leaves only text",
    command: {
        arguments: '<input>',
        options: [
            '-t, --title <title>', // title
            '-l, --lang <lang>' // language
        ]
    }
};

/** */
function processInput(inputPath) {
    const inputContent = utils.readFile(inputPath);
    const lines = inputContent.split('\n');

    let outContent = [];

    for (const line of lines) {
        console.log('~~line.length: ', line.length);
        if (line.length <= 1) {    
        } else if  (line.startsWith('00:')) {    
        } else {
            outContent.push(line);
        }
    }

    return outContent.join('\n');
}

/**
 *
 * @param {*} config
 * @param {ScriptContext} context
 */
const run = async (config, context) => {

    const params = utils.parseArgs(META, context.args);

    utils.logger.info(`Executing ${META.name} (%j)`, params);

    const inputPath = utils.getInputPath(config);

    const content = processInput(params.input);

    utils.generateFileFromTemplate(inputPath, __dirname, '../post/$post-item.md.njk', params, {author: '', body: content}, 'posts', {prependDate: true});
}

module.exports = {
    META,
    run
};
