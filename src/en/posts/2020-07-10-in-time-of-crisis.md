---
title: What to do in this time of crisis?

author: Young-Suk Ahn Park
date: 2020-07-10
tags: ["post"]
keywords: ["general"]

imageUrl: /images/posts/books-1617327_1920-poixabay.jpg
imageCredits: Image by <a href="https://pixabay.com/users/Marisa_Sias-526173/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1617327">Marisa Sias</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1617327">Pixabay</a>
---

The current COVID-19 crisis has drastically changed the life of all of us. The way we get
education, the way we work, the way we shop, the way we interact with others is very different
from how they were on early 2020. 

The situation has forced us to accelerate the adoption of online technology.
- Distance learning: School staffs are hastily adopting remote teaching practices. The academic institutions will try to retain students offering online courses.
- Remote working: Companies are adopting work from home policies, allowing employees to work from
wherever they are.
- Online shopping: There will be more online shopping. Even elders will learn how to shop online.
- E-government: Citizens will learn how to use use e-government services.
- Online entertainment: Number of subscribers to the streaming services and online gaming will 
increase even faster.
These changes are here to say, they will be the new norm. 

Unfortunately many companies unprepared to transition to online services will get deeply  
impacted. The degree of impact can vary depending on the sector, but the truth is everyone
has been affected in one way or the other. 

## "Never allow a good crisis to go to waste"[EMAN].
That is exactly the type of crisis we are living today. We do not have the luxury to let this
crisis go to waste.

This is the time to adapt, improve, become more agile and resilient. This is the time to clear our
mind and prepare ourselves to be able to see opportunities within the turbulence, use the tide in
our favor to push our boat to new horizons.


Indeed it is not easy to know what to prepare, even what to do next, but there are guides to help
us stay fit physically, mentally and emotionally allowing us to navigate though this crisis.


## Five Things to do During Crisis Time
The first rule is not to be too over ambitious. If we try to accelerate from 0 to 60 miles per hour in 
3 seconds, for sure we will burnout in no time.

This this [TEDx video](https://www.youtube.com/watch?v=TQMbvJNRpLE) Stephen Duneier correclty argues 
that to achieve a goal, the surest way is to apply marginal improvement. Going tiny step at a time.

That is the approach we should take to make small changes in our habits that make up our life.


### 1. Invest in your mental wellbeing
The uncertainty, the negative projection of the economy compounded with the social distancing
is creating a profound adverse effect in the psychology of the people [NCBI].

But we can prevent the development a mental diseases through simple activities. For example 
writing journals or small essays about your thoughts and feelings can be therapeutic.
Studies shows that writing a bout negative experiences helps your mind and your immune system
[PENN].

Likewise, meditating, or even just a simple act of taking few minutes with yourself, being mindful,
helps with the performance of your brain.  

I also suggest you to do a short retrospective before sleeping, praying at the end of the day is
actually a form of retrospect. Double point for you if you write down the reflection!

Another good suggestion is to listen to spiritual lectures or any other activity that nurtures your
mind and spirit.

And don't forget to spend quality time with your family! Time with your children and significant other
can be spiritually fulfilling. 


### 2. Exercise Daily
OK, I know you heard this before, several times. But with the lock-down order in place, it is
so easy to neglect our body. The mind and body is very closely connected. If your body suffers
your mind will suffer too. If your mind gets ill, your body will start deteriorating.

Now that the weather is getting warmer, get up early in the morning, while the air is still fresh,
and exercise. If you prefer to stay home because of the virus, you can still do push ups, planks, 
abdominal workouts, etc.

If it is difficult for you to make the habit, you can make it gameful, use the `if/when-then` technique. E.g. when I am done with restroom, I will do 20 push-ups.
Another good technique is Pomodoro, which has proven to be effective for increasing productivity: 
pause for 5~10 mins for every 25 mins of work. During the break you can do those small workouts. 


### 3. Grab Book and Read

The lock-down should have given you additional spare time in your day, as you no longer have to commute.
Those time saved, invest it reading books.

Reading elicits curiosity, increases knowledge and fosters imagination.

Reading a book takes time, therefore you would want to be be good at choosing books. There are good
sources that you can go to to find good books for you: book club, reviews, award-winnners 
(Pulitzer, Newbery, Caldecott, etc), classics, best-sellers, etc.

These are few of my recommendations:

- [Man's Search for Meaning, by Viktor E. Frankl](https://amzn.to/3fsZkPr)
- [The Alchemist, by Paulo Coelho](https://amzn.to/3iMNEsH)
- [The Circuit, by Francisco Jimenez](https://amzn.to/323WwUH)
- [Mindset, by Carol Dwek](https://amzn.to/38FijDI)
- [Emotional Agility, by Susan David](https://amzn.to/2Dpiyah)
- [Grit: The Power of Passion and Perseverance, by Angela Duckworth](https://amzn.to/3204VIY)
- [Any of the Chicken Soup for the Soul series](https://amzn.to/309lL5N)

These days, you can buy ebooks which give you the benefit of less paper wasted, no delivery needed,
and if you have a e-reader, you can carry many books without the weight that pains your back.
The [Kindle Paperwhite](https://www.amazon.com/gp/product/B07PQ96B6B/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B07PQ96B6B&linkId=e15d19e37dd4431eb800bb571199d165)
is a nice option, if you are avid reader, it may be better alternative than tablets as it is lighter,
cheaper, and since you cannot play video games there, you will have less distraction. 😉

If you would like to give your eyes some break, you can also listen to audio books. There are many
audiobook services such as [Audible](https://www.audible.com/), [libro.fm](https://libro.fm/), 
[scribd.com](https://www.scribd.com/), y [storytel.com](https://www.storytel.com/).

If you reside in United States of America, you may benefit from registering in a community library.
You can rent books and audio books for free using the [Libby](https://www.overdrive.com/apps/libby/) 
app. 


### 4. Practice Creating Thinking: Reframing, Repurposing, Lateral Thinking

If we stay most of our time in one single location, i.e. the confinement of the house, we will 
naturally fall to monotony, repetition of same daily pattern. These routine can yield shallow, 
banal thinking.

To avoid falling into this mindlessness, auto-pilot mode, we should practice activities suggested
for mindfulness. 

Try reframing. In any given situation, analyse possible causes and and implications, and try 
imagining the reverse, say considering the effects as the causes.  

Test yourself with the [challenge of 30 circles](https://www.ideo.com/blog/build-your-creative-confidence-thirty-circles-exercise).
The objective is to convert as many circles as possible to a recognizable objects. E.g. a circle be 
a face, another a pie, etc.

Make an habit these small internal mental challenges: reframing, repurposing, lateral thinking on 
every day life. Who knows, one of those thinkings could result in the next billing dollar business

If you need inspiration, the [TED.com](https://ted.com) videos are excellent source.

### 5. Interact with People

The COVID-19 has popularized the phrase "social distancing," but to be precise, we should call it
physical distancing. We should continue maintaining the social connection.

Based on the studies [WEBM], the effect of loneliness on the health is comparable to smoking 15 
cigarets per day. The gravity of COVID-19 is that the psychological damage that  the anxity can cause
is amplified by the social distancing.

The technologies allow us to keep the social interaction with families and friends. Tools such ass 
Google Meets, Zoom, WhatsApp, KakaoTalk, Skype, etc. can serve as alternative to physical interaction.

Notice that I have not mentioned social network such as Facebook, Instagram, TikTok and others, as 
those can cause adverse effects to loneliness [HARV].


### Bonus: Start a New Project
Since we are living a very unusual time, start a curious project that you have not done before, 
something uncommon!

Few ideas for inspiration:
- Learn a new language, this [TED blog explains you how](https://blog.ted.com/how-to-learn-a-new-language-7-secrets-from-ted-translators/).
- Builld your own personal hompepage. Today [publishing a website can be done with tiny investment, even free](https://creasoft.dev/es/posts/2020/2020-07-03-publicar_gitlabpages_con_dominio/).
- Learn programming, there are thousands of sites and videos avalable for free to tech you how to code in a language. 
- Write a book, your own autobiography! You can start small with a diary.
- Gather all those old magazines that you have finished reading and make a collage art.
- Or do an origami and create [hot pan base](https://www.youtube.com/watch?v=iL-sgxrP0eo).
- Start a side-business: translate, sell t-shirt, participate in [Mechanical Turk](https://www.mturk.com/)
- Use the vast content in internet to learn something, anything! cooking, quilting, design, piano,
 guitar, photography, video editing, drawing, self-defense, meditation, danding, yoga, you name it!
- Explore other [creative project ideas](https://daringtolivefully.com/creative-project-ideas)  

Well, that's how this site and [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw?view_as=subscriber) got started. 😎

REMEMBER, **start small, keep the rightm, be consistent**. A journey of a thousand miles begins 
with a single step.


## References
- [EMAN] Quote by Rahm Emanuel, the 56to mayor of Chicago.
- [NCBI] [Psychosocial impact of COVID-19](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7255207/), NCBI
- [PENN] Writing to Heal: A guided journal for recovering from trauma & emotional upheaval, James Pennebaker
- [WEBM] [Loneliness Rivals Obesity, Smoking as Health Risk](https://www.webmd.com/balance/news/20180504/loneliness-rivals-obesity-smoking-as-health-risk)
- [HARV][Does social media make you lonely?](https://www.health.harvard.edu/blog/is-a-steady-diet-of-social-media-unhealthy-2018122115600)

