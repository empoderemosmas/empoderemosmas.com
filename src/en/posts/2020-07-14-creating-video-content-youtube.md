---
title: Creating Video Content on YouTube

author: Young-Suk Ahn Park
date: 2020-07-14
tags: ["post"]
keywords: ["general"]

description: A quick instruction on how to create video from provisioning the equipments to filming and editing, to creating account in YouTube and publishing videos on it.

imageUrl: /images/posts/filmmaker-2838932_1920.jpg
imageCredits: Image by <a href="https://pixabay.com/users/lukasbieri-4664461/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2838932">Lukas Bieri</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2838932">Pixabay</a>
---

Since I started the YouTube Channel, many people have asked me questions regarding video content creation.

- How do I create a YouTube channel? 
- How do I create a video and publish it on YouTube?
- What tools do I need? What skills do I need?
- How do I monetize from my videos?
- How  do I live stream on YouTube?
- How much does all this cost?

In this article I will touch the surface on how to create content and publish it on YouTube. It’s a “starter” guide so you can get started, and learn as you go.


## First Thing First - Prerequisites

The great news is that there are not many requirements either on money or skill to get started. Nowadays, as long as you have a video camera - a phone with a camera is sufficient, access to the internet, idea and time, you can produce video and publish it on YouTube and other social platforms such as Facebook, Instagram, TikTok, LinkedIn, Viemo, and others.

So here are the minimum requirements:



1. **Access to the Internet**:  You will need to upload videos - which usually are large size. You will also probably be downloading stock photos/videos - those that you use as fillers in your video.
2. **Camera**: If you  own a camera that can take video at resolution of 1920 x 1080 pixels, then you are fine.
3. **Time**: Yup, the most precious  resource is time! You will need time to set up, learn, film, edit, publish, follow-up, all this requires time and dedication.

Now, let’s talk about additional cost. The cost depends on the level of fanciness, but you can definitely start with zero additional cost!

I strongly suggest you start small and enhance the equipment as you grow.

With the spirit of being agile, let’s jump straight to the meat.


## How to Produce a Video?

There are three phases in video creation:



1. **Pre-Production**: Writing the script, creating storyboards, scheduling, preparing cast, etc.
2. **Production**: The actual filming, creating animation, etc.
3. **Post-Production**: Editing the video; adding music; audio/video effects and adjustments; adding titles and credits; creating publicity materials, etc.

There are tools that can aid you in each phase.

For Pre-Production, I use [Google Docs]([https://docs.google.com/](https://docs.google.com/)) for writing the scripts and [Trello]([http://trello.com/](http://trello.com/)) for managing the taks.

Now, the production is where the action is! 

The bare minimum is a camera and a tripod. The microphone is highly suggested as the mic that comes with the camera (or the phone) will not produce the desired quality. The rest are optional that you can add one by one.


<table>
  <tr>
   <td>
   </td>
   <td><b>Minimum Budget</b>
- Essential
<p>
(below $50)
   </td>
   <td><b>Med Budget</b>
($100 ~ $1,000)
   </td>
   <td><b>High Budget</b>
($1,000 +)
   </td>
  </tr>
  <tr>
   <td>Camera
   </td>
   <td>Use your phone or existing camera
   </td>
   <td>Preferably with flip screen, like 
<p>
<a href="https://www.amazon.com/gp/product/B07TKNCQZL/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B07TKNCQZL&linkId=4de7d8d5a5b2ccb92b793b4c66ed38e2">Canon G7X</a> ($750)
   </td>
   <td>High-end 4K video camera
   </td>
  </tr>
  <tr>
   <td>Camera Stand
   </td>
   <td><a href="https://www.amazon.com/gp/product/B085QNKTX9/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B085QNKTX9&linkId=199a89cdeb7907e5249b5057fb784d8d">Tripod</a> ($26)
   </td>
   <td>
   </td>
   <td>Manfrottos tripod
   </td>
  </tr>
  <tr>
   <td>Microphone (recommended)
   </td>
   <td><a href="https://www.amazon.com/gp/product/B01M4J5WCM/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B01M4J5WCM&linkId=422586d0ecff92c32185f48580527114">Lavalier Mic</a> ($20)
   </td>
   <td>Wireless Lavalier Mic.
   </td>
   <td><a href="https://www.amazon.com/dp/B002VA464S/ref=twister_B07XHF2MLC?_encoding=UTF8&psc=1">Blue Yeti USB Mic</a>
   </td>
  </tr>
  <tr>
   <td>Camera Video Light
   </td>
   <td>Can do without
   </td>
   <td><a href="https://www.amazon.com/gp/product/B07T8FBZC2/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B07T8FBZC2&linkId=0192a033bc1376152eef392e2e6377f0">LED Video Light with Adjustable Tripod</a> ($~52)
   </td>
   <td><a href="https://www.amazon.com/gp/product/B01LXDNNBW/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B01LXDNNBW&linkId=442e00b1da9f1684f51457a56c2d3fde">Neewer Ring Light Kit</a>
($~100) and/or
<p>
<a href="https://www.amazon.com/gp/product/B016NDM99M/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B016NDM99M&linkId=c35d171ef10a8016ed2c9b787efaa81b">LimoStudio 2 Sets of LED</a> ($~160)
   </td>
  </tr>
  <tr>
   <td>Background screen (opt)
   </td>
   <td>Can do without
   </td>
   <td><a href="https://www.amazon.com/gp/product/B00E89Q5OY/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B00E89Q5OY&linkId=7216b1309270718ce1b4a9e360626be7">Chromakey Blue Collapsible Backdrop</a> ($~70)
   </td>
   <td><a href="https://www.amazon.com/gp/product/B07G7SV3ZP/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B07G7SV3ZP&linkId=c7d3dba226f9b515e32bab40026d3d9f">10 x 12ft Green Backdrop Screen</a> ($~45)
   </td>
  </tr>
  <tr>
   <td>camera stabilizer (opt)
   </td>
   <td>Can do without
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>


Depending on the characteristics of your video, other equipment may be more appropriate, for example if you are into action videos, [GoPro Hero7](https://www.amazon.com/GoPro-Silver-Elite-X-microSDHC-Adapter-UHS-I/dp/B07XZK2S9C/ref=sr_1_1_sspa?dchild=1&keywords=GoPro+Hero7&qid=1594701793&s=electronics&sr=1-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzUkxYWE1XNTFHTkUyJmVuY3J5cHRlZElkPUEwODE4NjY3M0gxS0lIUVA3RjY4MyZlbmNyeXB0ZWRBZElkPUEwODgwMjkzOTZUM09TNkZFNU8wJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==) ($220) could be a good camera. You may also need a camera stabilizer if your videos require moving.


## Filming

If you have taken videos and selfies with your phone, you already have experience!

The most difficult part of filming is managing your expressions and articulating clearly so that the audience have no problem listening to you.

Few simple tips



1. Prepare a script before filming, print it out and out the script and place it right below the camera (unless you can memorise the script).
2. Speak clearly.
3. Have sufficient lighting, but avoid direct light that casts shadows.
4. Put the camera at your eye level or slightly above, this usually gives a better angle.
5. Place the camera so that your hands and elbows are shown. You can always zoom in the editing.
6. Have a simple, clean background. Too many things in the background can distract the audience.

If you are using your phone camera, set the resolution to 1920 x 1080 or higher. The rear camera has a better lens, yielding a better quality. 

Now that you have your video, let’s move on to editing.


## Editing

Editing is where you add title, cut, stitch, color correct, and add other media files such as photos and music.

There are many editing software options from high-end professional edition to fee applications that come with the operating system.

If you have a Mac, you are already set, you can use [iMovie]([https://www.apple.com/imovie/](https://www.apple.com/imovie/)). You will just need to plug in your  camera or phone, copy over the files and start editing.

Microsoft used to make something similar called “Windows Movie Maker” but it got discontinued in 2017. There is a video editing capability in Microsoft Photos but is limited. [Movavi] claims to be the best iMovie alternative in Windows.

If you’ve been using a simple editor and you are looking for more advanced features, there are options:

- [Adobe Premiere Pro CC](https://www.adobe.com/products/premiere.html) - [Windows, Mac] Subscription based (US$20.99/mo)
- [Apple Final Cut Pro X](https://www.apple.com/final-cut-pro/) - [Mac Only] $299.99
- [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/) - [Windows, Mac, Linux] Free; Studio edition: $299
- [CyberLink PowerDirector](https://www.cyberlink.com/products/powerdirector-video-editing-software/overview_en_US.html) - [Windows Only] one time: $129.99, subscription: $4.33/ mo 
- [Adobe Premiere Elements](https://www.adobe.com/products/premiere-elements.html) - [Windows, Mac] $69.99
- [Corel VideoStudio Ultimate](https://www.videostudiopro.com/en/products/videostudio/ultimate/) - [Windows only]  $99.99

Nowadays there are even online video editing like [clipchamp.com](https://clipchamp.com/en/), [magisto.com](https://www.magisto.com/), [spark.adobe.com](https://spark.adobe.com/), and [wevideo.com](https://www.wevideo.com/).

Several years ago I did heavy investment on Adobe Premiere both on license and education, but this time I wanted to start with a low budget, I tried free open source softwares [OpenShot](https://www.openshot.org/) and [ShotCut](https://shotcut.org/) but ultimately decided on DaVinciResolve 16. So far I have been very satisfied with DaVinci. The free version comes with all the features I used to use in Premier.


### Gist of Video Editing

If you  are totally new to video editing, the basic process is as follows:



1. You import all media assets - the audio, video and still pictures - that you intend to use
2. You put  the assets on the audio / video tracks in time sequence.
3. You cut and reorder, and remove video clips
4. You put transitions in between video clips, eg. Fade In/Out, dissolves, and wipes.
5. You put background music
6. You overlay the video tracks with titles
7. You do color correction
8. You add special effects like motion tracking
9. And finally when all is done, you export to a specific format, usually H.264 codec in 1920 x 1080 or higher resolution.

Well, now you have a polished video, it's time to show it off to the world! 

## Creating a YouTube Channel.

YouTube is part of Google. If you do not have a gmail account, you wil need one to create a YouTube Channel.

With your gmail account, go to YouTube and you already have a personal channel.

If you want to create a brand channel, go to [https://www.youtube.com/channel_switcher](https://www.youtube.com/channel_switcher) and click on “Create a New Channel” it will create a new channel with the name you provide.

Now click on the video camera icon and upload your video.

Send me the link to your video, I'll be glad to be your audience.

## What Next?

There is more to video content creation, once you create your channel and start publishing your videos, you will need to market them if you want your video to reach a broader audience.

Marketing starts with proper content description, keywords and thumbnail image. 

Then when your videos start getting traction, you will start getting comments. For proper audience engagement, you should respond to the relevant comments.

If you want to reach to people speaking in other languages, you can add translations.

Then the cycle repeats, new ideas, new script, new video, marketing, engagement through comments.

And as you do so, you will learn more and more.

Be aware that you may get negative comments, and that is fine, there will always be people who will critique you and try to throw stones at you, but if you believe in your product, keep moving, keep sharing your idea!

Happy vlogging!
