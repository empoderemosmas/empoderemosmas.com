---
title: Retrospective 2020, year that deeply changed our lives

author: Young-Suk Ahn Park
date: 2020-12-31
tags: ["post"]
keywords: ["retro", "retrospective", "2020"]

videoUrl: https://www.youtube.com/embed/VS_IFm09Sig
---

2020 was a year that deeply changed our lives.

## Conflicts in the Globe
This year, many global conflicts shook the political sphere: Turkey and Syria, Azerbaijan  and Armenia, India and China, civil war in Yemen , and the Persian Gulf crisis. Those who suffered the most were the innocent civilians.

## Social Unrest
This year, acts of terrorisim in Nigeria and Afghanistan; national security law that China imposed on Hong Kong, the violence against women,  and the deaths of George Floyd, Breonna Taylor, Jacob Blake and other black citizens created waves of movements in the society across the world.

People raised their voice to demand their rights, to claim their unfairly taken dignity. And the march moves on.

## Wildfires
This year, wildfires turned millions of hectares into ashes, and billions of plants and animals that were once part of the beautiful natural wealth became dust.
The sky, covered with smoke, darkened, creating a vision of what would be our gloomy future.

One of the oldest tools of mankind, fire, became a warning sign that our civilization has gone too far.

## Extereme weathers
This year shattered records with the highest temperature in various points of the world;  and in some places, the temperatures dropped to the lowest in decades.
It was also the year with the most activity during the Atlantic hurricane season, and the year in which many countries in Asia and Africa have experienced the most devastating floods in decades.
The same water that is essential for life has demonstrated that it can also destroy and take lives.

From fire to flood, from extreme hot to cold temperatures, earth is sending us a message: “Enough!”

Earth raised her voice to demand respect, to make us think and be conscious of our uncontrolled actions.

## COVID-19
The latest of the warnings was COVID-19.

COVID-19 disrupted everything.
People from all over the world, regardless of gender, race, or social status were impacted by the virus.

In just a few months, the virus spread throughout the globe and produced a series of chain reactions affecting people’s physical and mental health, transforming industries and economies, changing the education system, and accelerating science and technology. COVID-19 changed the life of every single person on earth. Again, those who suffered most were the weak and marginalized people.

Because of our greed, our convenience, and indifference, we willingly chose to be blind and deaf to these manifestations.

## We Were Put To Test
2020 was a year of chaos, anxiety, loss and pain. It was a year where the family, the people in healthcare, the scientists, the governments - in short, humanity as a whole - were put to the test.  We held on, we worked hard, we learned. We became more compassionate and more agile. We opened our eyes and realized that heroes and angels were people like you and me.

The year 2020 left us with scars, but with it, valuable lessons. We learned that we are vulnerable , that we are all connected, that if we get together and focus we can achieve what had once seemed impossible.

We have demonstrated that we know 
how to raise our voice and fight against inequality, 
how to adapt and move forward, and 
how to collaborate and achieve wonders.

## Time to Reconcile
What we need to put into practice now is reconciliation. Reconciliation with oneself, with the family, with women and minority groups, with conflicting nations, and, more than anything, with nature.

Thanks to 2020, we learned that everyone's action compounds to tomorrow's outcome.

We will continue learning, innovating, rising;   and in the future, we will look back at 2020 as the stepping stone for humanity in building a better, brighter world.

> Dedicated to all the angels and heroes, those who silently fought with courage, those who kept their family afloat and helped  their neighbors.
And in honor to those who left us, may their soul rest in peace.


## References
- https://en.wikipedia.org/wiki/2020
- https://www.onthisday.com/events/date/2020
- https://nypost.com/list/major-2020-events/
- https://www.weforum.org/agenda/2020/12/good-news-stories-2020/
- https://www.cnn.com/interactive/2020/07/world/2020-year-in-review-july/

