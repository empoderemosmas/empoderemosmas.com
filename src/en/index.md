---
title: Empowerplus - Home
layout: layouts/page_with_posts.njk

disableComments: true

pagination:
  data: collections.posts_en
  size: 10
  reverse: true
  alias: posts

description: Empoderemos Mas, personal growth, professional development y social betterment
keywords: ["personal growth", "self-help", "professional", "betterment"]

---
<div 
    style="background-image:
           url('/images/em-background.jpg'); 
    height:200px;
    background-size: 100%; 
    background-position:center;">&nbsp;</div>

# Empowerment is not an option, it's a responsibility!

## So many challenges ahead. What are you waiting for to get empowered?

Personal growth, professional development, social betterment.