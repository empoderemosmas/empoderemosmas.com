---
title: Partnership
---

# Why be a Partner?
If you are passionate about giving back to the community by sharing your knowledge and empowering 
others, join us!

Every time we create a quality content, we are not only helping other grow but also reinforcing our 
knowledge and sharpening our communication skill, a core competency that every person should have.

# How to become a partner?
Send us an email to empodermosmas@gmail.com with your skills, expertise and interest that you want
to share. We will help you create the content in form of videos and articles.
