---
title: ¿Qué debemos hacer en este tiempo de crisis?

author: Young-Suk Ahn Park
date: 2020-07-10
tags: ["post"]
keywords: ["crisis"]

imageUrl: /images/posts/books-1617327_1920-poixabay.jpg
imageCredits: Image by <a href="https://pixabay.com/users/Marisa_Sias-526173/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1617327">Marisa Sias</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1617327">Pixabay</a>
---

La situación del COVID-19 ha cambiado drásticamente la vida de muchos. La manera como estudiamos o
trabajamos como hacemos las compras, como interactuamos con otros es muy distinta a como era hasta
principios e 2020.

Muchas de estas forzadas transformaciones en la sociedad será la nueva norma. El común denominador de los cambios es el aprovechamiento de las operaciones en línea.
- Educación a distancia: Las instituciones educativas tratarán de retener los estudiantes ofreciendo educación a distancia. 
- Trabajo remoto desde la casa: Las empresas adoptaran la política de trabajo remoto, permitiendo 
a sus empleados trabajar desde cualquier lugar.
- Compras en línea: Las personas utilizaran más las compras por e-commerce.
- Tramites en linea: Los ciudadanos aprenderán a utilizar los servicios de trámite provistos en internet.
- Entretenimiento en línea: Aumentará el número de suscriptores a servicios de streaming y juegos
en-linea.

Desafortunadamente muchas empresas que no están preparados para proveer servicios virtuales y 
remotos quedarán afectados de manera negativa.

Dependiendo del sector, el impacto puede ser variado, pero lo cierto es que que hay sector que
no haya sido afectado de una manera u otra.  

## "No dejes que una crisis seria se desperdicie"[EMAN].
Es justamente el tipo de crisis que vivimos hoy día cual no podemos darnos el lujo de desperdiciar.

Este es el tiempo para adaptarnos, para mejorarnos, para volvernos más resistentes y expandir 
nuestras capacidades. Es el tiempo de aclarar nuestras mentes para poder ver oportunidades dentro de la turbulencia, utilizar la marea para dirigir nuestro barco a nuevos horizontes.

Es cierto que no es fácil saber que hacer, que acciones tomar para prepararnos, pero si existen 
consejos para guiar.

## Cinco cosas que debe hacer durante la crisis
La regla número uno es no tratar de ser muy ambicioso. Si tratamos de acelerar de cero a 100 km/hora en 3 segundos, de seguro nos agotaremos rápidamente no llegaremos muy lejos.

En este [video de TEDx](https://www.youtube.com/watch?v=TQMbvJNRpLE) Stephen Duneier explica como
el método de pequeñas mejoras le a ayudado en alcanzar grandes metas, trocito por trocito.

Es justamente lo que debemos hacer, realizar pequeños cambios que necesitan poco esfuerzo para empezar y mantener. Y continuar haciendo.


### 1. Invierta en su bienestar psicológico
La incertidumbre, las proyecciones negativas de la economía combinado con el distanciamiento
social esta creando un efecto adverso profundo en la salud mental de las personas[2].

Podemos prevenir el desarrollo de enfermedades mentales con actividades que no son difíciles.
Escribir diarios o pequeños trozos de lo que siente y lo que está en su pensamiento es en si 
una terapia. Estudio indica que escribir sobre experiencias negativas mejora tanto el sistema 
inmunológico como la mente.

Asimismo la práctica de meditación, o solo el acto de tomar unos minutos con su propia conciencia
mejora el rendimiento de la mente.

También le sugiero que tome tiempo de reflexionar antes de dormir, y si escribe su reflexión 
doble puntaje!

En los fines de semana escuche sermón o realice alguna otra actividad que nutra su 
espiritu.

Y claro esta, pase tiempo de calidad con su familia. 

### 2. Haga ejercicio diario
De ninguna manera descuide su cuerpo. La mente y el cuerpo está estrechamente conectado.
Si el cuerpo sufre, su mente sufrirá, y cuando la mente sufre también el cuerpo empezará
a deteriorar.

Levántese temprano en la mañana, cuando el aire aún es fresco, y haga ejercicio. Si la cuarentena
no le permite salir a ejercitar, haga saltos de cuerda en la casa, o push-ups. Introduzca
varios sesiones cortos de ejercicio durante el día. Es recomendable pausar el trabajo cada
25 minutos (metodo pomodoro) para tener mayor enfoque. Durante esa pausas ejercite.

Si le es difícil acordar hacer ejercicio, póngase un auto-programa de 'si-entonces', 
por ejemplo: si al terminar de hacer las necesidades en el bano, entonces hago 15 push-ups. 


### 3. Cultive el hábito lectura

Probablemente la cuarentena le ha aumentado tiempo libre. Esa  hora y media que ahorro por no tener que viajar a la oficina, invierta en lectura.

La lectura induce curiosidad, aumenta conocimiento y fomenta imaginación.

Leer un libro toma tiempo, por lo que hay que saber seleccionar buen libro. Las recomendaciones de lectores entusiastas, libros clásicos, selecciones de libros en artículos de revistas e internet, lista de los más vendidos, son buenos fuentes.

Personalmente le recomiendo estos libros si no lo ha leido aun,

1. [El hombre en busca de sentido, por Viktor E. Frankl](https://amzn.to/3fsZkPr)
2. [Cajas de Cartón, por Francisco Jimenez](https://amzn.to/323WwUH)
3. [El Alquimista, Paulo Coelho](https://amzn.to/3iMNEsH)
4. [Mindset, La actitud del Éxito, por Carol Dwek](https://amzn.to/38FijDI)
5. [Agilidad Emocional, por Susan David](https://amzn.to/2Dpiyah)
6. [Grit: El poder de la pasión y la perseverancia](https://amzn.to/3204VIY)
7. [Serie de Sopa de pollo para el alma](https://amzn.to/309lL5N)


Los libros, en vez de comprar físicamente se puede comprar electrónicamente y leer con 
tableta or e-reader. El [Kindle Paperwhite](https://www.amazon.com/gp/product/B07PQ96B6B/ref=as_li_tl?ie=UTF8&tag=empoderemosma-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B07PQ96B6B&linkId=e15d19e37dd4431eb800bb571199d165) es una buena 
alternativa a tabletas siendo más liviana y más económica que las tabletas pero con el limitante 
que no podrás jugar videojuegos en ella - aunque este limitante puede ayudar a evitar distracciones.

Si prefieres descansar tus ojos, puedes optar por audiolibros. Existen varios servisio como el 
[Audible](https://www.audible.com/), [libro.fm](https://libro.fm/), 
[scribd.com](https://www.scribd.com/), y [storytel.com](https://www.storytel.com/es/es/)

En EE.UU. al registrarse en la biblioteca de su comunidad usted puede alquilar gratis libros electrónicos y auidiolibros utilizadno el app [Libby](https://www.overdrive.com/apps/libby/)


### 4. Practique replanteamiento y cambio de perspectivas

Ya que estamos encerrados en la casa, si no nos esforzamos la tendencia es la monotonÍa, repeticiones de patrones día tras día. Esta rutina puede resultar en pensamientos superficiales y poco creativos.

Para evitar caernos en estado de pérdida de atención y autopiloto, debemos practicar actividades sugeridas para atención plena (mindfulness).
Una de ella es el replanteamiento, en una situación dada, analice los posibles causantes e trate de imaginar invirtiendo la causa por los efectos o trate de ver desde nueva perspectiva.

Hágase una prueba con el [reto de 30 circulos](https://www.ideo.com/blog/build-your-creative-confidence-thirty-circles-exercise).
La idea es convertir los 30 círculos en objetos reconocibles dibujando en ella (ej. Un círculo puede ser una cara).

Convierta en hábito estos mini retos mentales de replanteamiento (reframing) y readaptación (repurposing) de las cosas cotidianas y situaciones. Una de ellas puede resultar en una idea de negocio exitoso!

Si necesita inspiracion, los videos de [TED.com](https://ted.com) podran ser de ayuda.

### 5. Conéctese con las personas

El COVID-19 ha puesto popular el término “distanciamiento social”, pero para ser preciso, lo que se necesita no es el distanciamiento social sino el distanciamiento físico.

Basados en los estudios [WEBM], la soledad causa daños a la salud comparables a 15 cigarrillos diarios. Lo peligroso del COVID-19 es el daño psicológico que puede causar la angustia por las noticias amplificada por el aislamiento social.

Con las tecnologías que tenemos, podemos mantener las interacciones con los familiares y amigos. Google Hangout, WhatsApp, Zoom, KakaoTalk, Skype. Todas estas herramientas aliviar el impacto de la desconexión social.
Nótese que no he mencionado redes sociales como Facebook, Instagram, TikTok, u otros, cuales puede tener efecto adverso[HARV].

### Extra: empiece un proyecto nuevo
Ya que estamos viviendo un tiempo muy inusual, empiece algo nuevo que no ha hecho anteriormente, algo insólito.

Estas son algunas ideas:
- Aprenda un nuevo lenguaje, este [blog de TED en inglés muestra cómo](https://blog.ted.com/how-to-learn-a-new-language-7-secrets-from-ted-translators/).
- Produzca su página personal de web. Hoy día se puede [publicar sitio web es en un costo muy reducido](https://creasoft.dev/es/posts/2020/2020-07-03-publicar_gitlabpages_con_dominio/).
- Aprenda a programar. Sitios como [creasoft.dev](https://creasoft.dev/es/) sera de ayuda.
- Escriba un libro, su autobiografia! Puede empezar con un pequeño diario.
- Recoja todas las revistas antiguas y crea un obra de arte collage.
- O produzca [base para ollas calientes](https://www.youtube.com/watch?v=iL-sgxrP0eo).
- Empiece un negocio secundario: traduciendo, vendiedo articulos.
- Tome ventaja de internet como el Massive Open Online Courses (MOOCs) - coursera.org, edx.org, udemy - para aprender lo que siempre quiso aprender: cocinar, tejer, diseño, piano, guitarra, fotografía, edición de video, dibujo, autodefensa, meditación, baile, yoga, 
- Explore otras [ideas creativas](https://daringtolivefully.com/creative-project-ideas)

Asi surgio el canal de  [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw?view_as=subscriber) y este blog. =)

Recuerde, **empiece pequeño, mantenga el ritmo, y sea consistente**. El viaje de mil kilómetros se logra con un paso a la vez.


## Referencias
- [EMAN] Frase que dicho por Rahm Emanuel, el 56to alcalde de Chicago.
- [NCBI] [Psychosocial impact of COVID-19](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7255207/), NCBI
- [PENN] Writing to Heal: A guided journal for recovering from trauma & emotional upheaval, James Pennebaker
- [WEBM] [Loneliness Rivals Obesity, Smoking as Health Risk](https://www.webmd.com/balance/news/20180504/loneliness-rivals-obesity-smoking-as-health-risk)
- [HARV][Does social media make you lonely?](https://www.health.harvard.edu/blog/is-a-steady-diet-of-social-media-unhealthy-2018122115600)

