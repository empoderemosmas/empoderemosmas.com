---
title: Avances digitales forman un arma de doble filo

author: Young-Suk Ahn Park
date: 2020-06-28
tags: ["post"]
keywords: ["general"]

videoUrl: https://www.youtube.com/embed/vEvT6JkCFYA
---

Esta es la realidad de hoy: 
Carros que manejan solos; ingeniería genética; relojes que te avisan tu nivel de glucosa; algoritmos que generan imágenes fotorealistas; bocinas que responden a tus comandos y te hablan de vuelta; drones que vigilan ciudades; robots que hacen piruetas, realidad virtual, realidad aumentada, inteligencia artificial! 

Estamos viviendo en un tiempo sumamente fascinante!

El tema de esta sesión es el Avance digital, veamos cómo estas herramientas, cuales tomamos por hecho de manera cotidiana, forman un arma de doble filo.

Yo cuando me levanto en la mañana, le digo a Alexa que me reproduzca la música de mi agrado mientras  preparo el desayuno. Trabajo desde la casa haciendo reuniones virtuales. Y colaboro en tiempo real con mis colegas utilizando documentos electrónicos que están en la nube (cloud).

En los fines de semana produzco estos videos utilizando la cámara de mi celular y edito con un software gratuito. Al terminar la edición, subo el video a la nube y con presionar un botón la inteligencia artificial de la plataforma me genera de manera automática la transcripción y traduce al inglés y al coreano, sólo tengo que verificar que la traducción esté correcta.

Asimismo hoy dia, veo:
Cómo los celulares hoy día tienen mayor capacidad que la computadora de tamaño de microondas que usaba en mi vida universitaria.  Y que estos dispositivos permiten hasta detectar el cáncer de la piel.
Cómo las personas utilizan reloj de pulso para medir su salud, comunicarse con otras personas y gestionar las tareas del dia. 
Cómo al tocar el timbre de una casa, el dueño que se encuentra de viaje a miles de kilómetros, me observa por la cámara y me habla.
Y al presionar un botón en el móvil, en minutos viene un transporte para recogerme y llevarme a mi destino.
En el trabajo me doy cuenta sobre softwares crean efectos visuales en tiempo real para películas.
En las noticias veo como Robots que manejan inventarios.
Como Las personas se juntan gracias a las redes sociales, no solamente para hacer fiesta sino también para aprender, recoger basuras en las costas y protestar contra la injusticia social.
El servicios en Google entabla conversaciones con humanos sin que ellos se den cuenta que están conversando con algoritmos. 
Y cómo las empresas privadas están lanzando al espacio cohetes con personas.

Lo que anteriormente parecía locura, ahora es natural ver un bebe tratando de mover la imagen de una revista como si fuese una pantalla táctil de una tableta. O ver personas conversar con televisores y refrigeradores.

Todo esto es posible debido a la digitalización. La combinación de alta capacidad de procesamiento, capacidad masiva de almacenamiento de datos y transmisión de datos super rápida, abre la puerta a innumerables oportunidades que anteriormente se habían imaginado solo en películas de ciencia ficción.

Si proyectamos un poquito, no es nada difícil encontrar nuevas oportunidades:
Nano dispositivos para atacar células cancerosas,
Sistemas con realidad aumentada para realizar cirugías remotas. 
Robots para salvar vidas al ocurrir desastres.
Manos, pies, piernas artificiales que funcionan como si fuesen reales
Educación personalizada virtual que llega hasta lugares remotos

El impacto de la digitalización es clara. La sociedad de hoy ya no se puede separar de la digitalización.

Cuando analizamos más nos damos cuenta que la digitalización es una arma de doble filo: así como trae beneficios y oportunidades, también trae riesgos y peligros como la adicción, pérdida de trabajo, pérdida financiera, entre otros. Solo imagínese lo que pueden hacer personas con intenciones maléficas: desde chantaje virtual hasta manipulación social.

Es imposible saber a fondo la ciencia y los mecanismos de todas estas tecnologías, pero sí es primordial conocer los conceptos al punto de que podamos discernir y razonar. El conocimiento es nuestra herramienta más poderosa no solo para protegernos, sino para aventajar (crecer).

En las próximas sesiones elaboraré más este tema. Hablaré primero sobre los conocimientos indispensables para salvaguardar de los riesgos del mundo digital y luego exploraré las oportunidades que nos trae esta continua digitalización para poder tomar ventaja de ella. 

Continuemos aumentando nuestros conocimientos para que sirvan de base para incrementar nuestra capacidad y empoderarnos más.

<div class="fb-comment-comment" data-href="{your-comment-url}" data-width="500"></div>