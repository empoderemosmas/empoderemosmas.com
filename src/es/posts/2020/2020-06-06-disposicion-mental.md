---
title: Disposición mental y emocional

author: Young-Suk Ahn Park
date: 2020-06-06
tags: ["post"]
keywords: ["general"]

videoUrl: https://www.youtube.com/embed/Ztsii_xCK1k
---

## Preámbulo

Los obstáculos más grandes que nos impiden alcanzar nuestros objetivos no es la escasez de recursos financieros, tampoco la insuficiencia de poder o la falta de tiempo, sino la falta de disposición psicológica: mentalidad resistente y agilidad emocional.

Solamente piensen en todas esas ocasiones en donde usted ha dejado pasar las oportunidades ya sea debería

por timidez, miedo, u otras razones están basados en meras suposiciones.

Muchos son los intentos que no se llevan a cabo por suposiciones no verificadas.

Vemos algunos casos comunes:



*   El dibujar que tanto le encantaba en la niñez,  pero que después de haber visto a  otros dibujar mejor,hizo que dejara este interés.
*   La chica o el chico que le gustaba al que nunca le preguntó si quería salir, asumiendo que ella o él está “fuera de tu alcance”
*   Las oportunidades de trabajo o aumento de salario que dejó pasar por miedo a preguntar y a ser  rechazado.
*   La idea de negocio que nunca se materializó después de haber escuchado unas cuantas críticas.
*   El salto que no ha dado aún por terror a caer.

Tantos son los complejos meramente psicológicos que nos detiene y nos dejan atascados!

En mi caso, uno de mis grilletes es la comparación con las personas conocidas.  

Desde la secundaria empecé a creer que los adultos me comparaban con otros estudiantes más talentosos. Al principio me llegaba como un desafío, pero poco a poco se fue convirtiendo en una sumisión que me llevó a pensar que no tenía control de mi capacidad y que estaba marcado.

Mi traslado a otro país para estudios universitarios me dio la oportunidad de verificar que la  suposición que yo había creado no era válida.

Ahora, después de haber tenido experiencia como empresario, haber completado una maestría en una prestigiosa universidad en los EE.UU. seguir vivo, con 17 años de casado y con una hija de 11 años,  puedo decir que he formado mis propios valores y creado mi propia filosofía.

Aun así sigo luchando contra estos malos hábitos, pero el aprender sobre la mente y las emociones y entender los porqués me ha ayudado significativamente a tener una vida más enfocada, productiva y satisfactoria.


## La mente y las emociones

Como mencioné al inicio, son innumerables las ocasiones en que nos echamos cuentos a nosotros mismos, muchas veces con tendencias negativas, simplemente para evitar la incómoda sensación de la incertidumbre.

Esto afecta nuestra manera  de cómo percibimos e interactuamos con el mundo, y determina la calidad de nuestro bienestar psicológico.

Cada mente es un universo inmensamente complejo.  Debido a la complejidad e intangibilidad, el campo de la ciencia y medicina  ha puesto poca inversión en ella, por ende son pocos los estudios sobre la mente comparados con otros campos de medicina.

En la década reciente, con el aumento de los trabajos basados en conocimiento (knowledge work), consultores, diseñadores, desarrolladores de software, empresarios, etc, ha surgido el interés en entender cómo la mentalidad y manejo de las emociones afecta directamente a la salud, rendimiento profesional, hasta el nivel de felicidad y calidad de vida, no solamente de individuos sino de grupos y sociedades.

Un estudio riguroso en el empresa Google sobre la efectividad de los equipos [[GOOG15](https://rework.withgoogle.com/blog/five-keys-to-a-successful-google-team/), [HBR17](https://hbr.org/2017/08/high-performing-teams-need-psychological-safety-heres-how-to-create-it)] ha reafirmado que el bienestar psicológico es, en efecto, el factor número uno para el alto rendimiento.

Ahora, enfoquémonos en el aspecto más poético, pero poderoso: las emociones. Si han escuchado la frase "la pasión mueve el mundo" bueno, las emociones son los pistones, que mantiene el movimiento.

Cuando uno menciona la palabra “emoción,” las personas creen entender su significado, pero lo interesante es que aún no hay un consenso en la definición científica para dicho término [[WIKI](https://en.wikipedia.org/wiki/Emotion), [ATLA2015](https://www.theatlantic.com/health/archive/2015/02/hard-feelings-sciences-struggle-to-define-emotions/385711/)], esto indica lo complejo que es el mundo de las emociones.

Emoción viene del latin ēmovēre, que significa poner en movimiento, o sea, lo que nos mueve.

En wikipedia se define como las: “reacciones psicofisiológicas que representan modos de adaptación a ciertos estímulos del individuo cuando percibe un objeto, persona, lugar, suceso o recuerdo importante. Psicológicamente, las emociones alteran la atención, hacen subir de rango ciertas conductas guía de respuestas del individuo y activan redes asociativas relevantes en la memoria.”

Una persona puede proyectar un gama de emociones. De acuerdo a la teoría de la evolución, las emociones son estímulos sensoriales que nos ayudan a adaptarnos y a sobrevivir.

Nuestros antepasados tenían que proteger su ración de comida y protegerse de depredadores como tigres, hienas y lobos.

Al enfrentarse a un oso, la mente de una persona solo piensa en una cosa: cómo sobrevivir, o luchas o huyes. [En ese momento, que si el vecino de al lado, que si tu suegra, compañero de trabajo… ya no es importante..] Gracias a estos estímulos el cuerpo se prepara, las glándulas sudoríparas se activan y el sudor permite que las manos tengan un mayor agarre, y el sudor en los pies evita tropiezos al correr (miles de años atrás no existían las zapatillas). Y los latidos del corazón proveen mayor energía al cuerpo.

De manera similar, funciona al percibir una amenaza en la noche, uno no puede dormir para poder reaccionar de inmediato ante el peligro .

Todas estas reacciones que llevamos son programas cuales han sido codificado en nuestros genes por miles de años para ayudarnos a sobrevivir y mantener nuestra especie.

Analicemos una situación en la vida social de hoy. Cuando nos paramos en frente de la gente para presentarnos, la mente con nuestra emoción primitiva actúa como si estuviésemos en frente de un depredador y nos dice, “Pelao, [regionalismo panameño]  ¡corre que estás en peligro!” y nuestro corazón late fuerte y sudamos.

En la actualidad, los contextos han cambiado, vivimos en una sociedad más elaborada y aun así,  esas reacciones siguen existiendo pero ya no se justifican, y muchas veces causan estorbos, obstaculizando pensamiento más claros, pues la supervivencia de hoy es mucho más compleja. El peligro no es el león en frente tuyo, sino tu pareja que está al lado y te tira una mirada amenazante.

Veamos el caso del “temor”, que es una emoción que nos acecha cuando menos lo necesitamos. 



*   Fíjese que le pasa a los jóvenes cuando están en frente de una persona que sienten atraídos. Se ponen nerviosos, a veces hasta meten la pata. [Regionalismo panameno]
*   ¿Que nos ocurre en las entrevistas de trabajo? La mente se nos blanquea y no respondemos correctamente aún cuando sepamos la respuesta correcta.
*   Y ¿qué tal cuando nos paramos en frente de muchas personas para presentar algo? nos suda la palma de las manos y el corazón empieza a latir rápidamente y tartamudeamos.
*   Que nos ocurre cuando ¿la empresa está en crisis? No podemos dormir pensando en los miles de posibles catástrofes que nos puede ocurrir.


### Hoy día, para que nos sirven las emociones?

Si en el pasado las emociones eran herramientas de supervivencia. Hoy en día son indicadores. Debajo de las emociones, especialmente las que más nos cuesta, las que más sentimos puntiagudos, son las que nos señalan las cosas que más nos importan.

Por ejemplo, si lees las noticias y un artículo sobre jóvenes que no pueden asistir a los colegios por desigualdad social te enfurece, significa que valoras la justicia social.

O si sientes aburrimiento en el trabajo, que llega al punto de estrés, puede significar que valoras tu crecimiento y necesitas más retos, pero no estás encontrando oportunidades en el trabajo actual.

El corolario es que no sentimos emociones a las cosas que poco nos importa. 

Y esto nos ayuda a enfocar nuestros esfuerzos en asuntos de valor.

Pero hay que tener cuidado al interpretar las emociones, pues ellas nos puede dejar caer en nuestras propias trampas.  De acuerdo al renombrado Dr. David Burns de la universidad de Stanford, nuestra mente nos chantajea con pensamientos negativos, los llamados distorsiones cognitivas:

Estas son las diez distorsiones cognitivas:



1. Pensamiento Polarizante (todo-o-nada): Como no soy perfecto, soy un fracasado.
2. Sobregeneralizaciones: Si alguien te rechaza te dices a ti mismo que nadie te ama y eres un perdedor.
3. Filtro Mental: Toma un aspecto negativo y lo magnificas, y solamente piensas en ella. Una foto que pusiste en el Facebook, trajo montón de comentarios agradables, pero si un comentario dice que te ves más vieja, . ya no puedes dejar de pensar en ese comentario
4. Descartando los hechos: cuando alguien te da cumplidos piensas que solo lo está haciendo para que tu te sientas bien.
5. Conclusiones precipitadas: Tu amigo no te ha devuelto la llamada en un dia, asi que te esta ignorando
6. Magnificacion (Catastrofante) o Minificacion : El desastre siempre está inminente. Tu jefe te vio un una vista, porque está pensando en despedirte.
7. Razonamiento Emocional: Me siento como derrotado, así que debo ser un perdedor.
8. Obsesión con “Debería”: Existen variaciones, pueden ser autodirigidos: todas esas frases que empieza con “**No debí** …”, “**Si tan solo hubiera**”, O también dirigidos a otros: “**Tu tienes que**...”. Dirigidos al mundo; “**Las personas no deberían** ...”
9. Etiquetando: “Soy un torpe”, “él es un ingenuo”, ...
10. Culpando: Culpandose a sí mismo, y/o culpando a otros. 

Si podemos aprender de nuestras emociones, entender lo que nos intentan decir, sin que nos nuble el pensamiento objetivo, podemos usar la información de manera increíblemente útil. Ayudándonos a avanzar hacia nuestras metas y valores.

En las próximas sesiones compartiré sobre el concepto de la agilidad emocional de la Dra. Susan David de la Universidad de Harvard, para que sepamos utilizar las emociones a nuestro favor.

Y el concepto de la mentalidad del crecimiento (Growth Mindset) por la Dra Carol Dwek de la Universidad de Stanford.

Tengamos una mentalidad más resistente y sana!

Y sigamos Empoderandondos Más, ya que nuestra capacidad aún está por crecer. 

y nos vemos en la próxima!


## Referencias

- [John_Gottman, Wikipedia](https://en.wikipedia.org/wiki/John_Gottman)
- [Theories of emotion](https://www.verywellmind.com/theories-of-emotion-2795717)
- [Theories of Emotion, Lumen Learning](https://courses.lumenlearning.com/waymaker-psychology/chapter/emotion/)
- [Las emociones](http://www.sagepub.net/isa/resources/pdf/Emociones.pdf)
