---
title: Resumen de COVID-19

author: Young-Suk Ahn Park
date: 2020-05-02
tags: ["post"]
keywords: ["general", "covid", "pandemia", "coronavirus"]

videoUrl: https://www.youtube.com/embed/FTNhgYbk3Us
---

La situación del COVID-19 es una situación sin precedentes, que ha afectado drásticamente la vida de tantas personas, en muchos lugares, en tan corto tiempo. El COVID-19 sigue impactando casi todos los sistemas existentes.


> Países afectados: (210 paises y territories, 4/11/2020 → 213 territorios  ciertos países no han reportado). 3.1 M de afectados 
(Fuente fiable: Prof. Kim Asian Boss video 1, video 2 (es))


En esta sesión compartiré un resumen de COVID-19 desde el punto de vista médico. 


> Virus no se categoriza como organismo viviente, por ende "morir" en este contexto se refiere que el virus ya no es capaz de infectar.



## ¿Que es COVID-19?

El COVID-19 (**Co**rona**vi**rus **D**isease **2019**) es la enfermedad respiratoria causada por una variación de coronavirus identificado en 2019, denominado SARS-Cov-2, un virus de tipo RNA. 

Es de la misma familia del SARS-Cov. (SARS es las sigla para Síndrome Respiratorio Agudo Grave en Inglés “Severe Acute Respiratory Syndrome”).

> COVID-19 es la enfermedad y coronavirus es el virus. 


El nombre de coronavirus se debe por su forma de corona cuando es visto por microscopio electrónico. 

Existen [7 tipos de coronavirus identificados que infectan humanos](https://www.goodrx.com/blog/what-does-covid-19-mean-who-named-it/), [cdc](https://www.cdc.gov/coronavirus/types.html) cuales causan enfermedades con síntomas en el tracto respiratorio y gastrointestinal.


## ¿Como surgió?

En diciembre de 2019, la oficina de OMS en China recibió reportes de varias personas con casos de neumonía sin causa conocida en la ciudad de Wuhan, provincia de Hubei.

En enero, científicos de China pone a disposición la secuencia genética del nuevo coronavirus.

Y en marzo la OMS categoriza COVID-19 como pandemia.

[[WHO historial](https://www.who.int/news-room/detail/08-04-2020-who-timeline---covid-19)].

Los análisis indican que el virus que existía originalmente en murciélagos, se propagó a los seres humano. La propagación fue posiblemente mediante huéspedes intermediarios como pangolines o serpientes. 


## ¿Como se transmite?

La enfermedad se transmite por contacto de fluido corporal infectado. El caso más común son las las gotitas respiratorias que viajan por el aire al toser, estornudar, cantar o hasta hablar.  La infección ocurre cuando las gotitas caen en la fosas nasales, boca u ojos. 

> Las gotas por los estornudos pueden alcanzar hasta 2 metros y mantenerse en el aire por largo periodo de tiempo. Por ende se hace necesario un distanciamiento de 2 mtrs.
[average sneeze or cough can send around 100,000 contagious germs into the air at speeds up to 100 miles per hour]


Otra forma de contraer el virus es teniendo contacto con las superficies infectadas con virus y luego frotarse la boca, nariz u ojos.

El tiempo que el virus puede mantenerse “vivo” depende del material de la superficie. En superficies duras como tabla de madera y metal, el virus puede sobrevivir hasta por 3~4 días. El virus tiene una vida más corta en las fábricas y los papeles pues dichos superficies absorben el agua del virus dejándola inactiva.

La razón por la que  el virus se infecta por la boca, nariz u ojos es debido a que ellos tienen membranas mucosas con abundancia de la enzima ACE2, cuya función normal es de regular la presión de la sangre, pero el virus la utiliza como “túnel” (clínicamente llamado receptores) para entrar a nuestras células y reproducirse.


> La piel no tiene estos receptores protegiendo de gérmenes. {El cuerpo es fascinante}


También se han dado casos de humano transmitiendo el virus a otros mamíferos. Tenemos como ejemplo casos de gatos y hasta tigres en NY. 


> Los casos de gripe porcina y gripe aviar en el pasado fueron ocasionados por virus (influenza) transmitidos desde otros mamíferos y aves.


Una de las razones por la que el COVID-19 se ha propagado tan rápidamente es debido a que existen personas sin síntomas que pueden transmitir la enfermedad. 

Puede darse dos casos: el primer caso son las personas asintomáticas a quienes no se observan síntomas pero transmiten; el otro caso son las personas que están contagiadas y están en los dos primeros días que no presentan síntomas pero transmiten. 

Se ha descubierto en heces virus con más de 23 días.

En Corea del Sur, hay paciente que está en su 52vo dia de recibir tratamiento. Personas con debilidad en sistema inmunidad retendrá virus por tiempo más prolongado.

Inmunidad contra COVID-19 signifca tener anticuerpo necesario para dicho virus.


## ¿Cuales son los síntomas y efectos?

Los Centros para el Control y la Prevención de Enfermedades (CDC) de los EE.UU. indica como síntomas:  

1. Fiebre, 
2. Tos, 
3. Dificultad para respirar (sentir que le falta el aire), 
4. Temblores y escalofríos que no ceden,
5. Dolor muscular, 
6. Dolor de cabeza, 
7. Dolor de garganta, 
8. Pérdida reciente del olfato o el gusto

También se ha reportado sarpullido en la piel, y pérdida de apetito.

El COVID-19 puede causar complicaciones en los sistemas respiratorios y circulatorio. En situaciones graves puede llevar hasta la muerte.

Las personas con mayor riesgo de complejidad son aquellos con condiciones médicas crónicas, problemas cardiovasculares y pulmonares como diabetes, obesidad, hipertensión, inhalación de nicotina (fumadores). También están en riesgo las personas que están tomando inmunosupresores como esteroides, y aquellos que están en tratamiento de cáncer.

Mucho de los casos graves puede tener relación con el fenómeno llamado tormenta citosina (Cytokine storm) en donde el sistema inmunológico ataca tan agresivamente a los invasores que daña hasta células de nuestro propio cuerpo (daños colaterales). Esto provoca inflamación, desarrollando neumonías y llevando a disfunción de los órganos. 

Otros [reportes](https://science.sciencemag.org/content/368/6489/356.full?fbclid=IwAR2O0dlXMQMfyPQ-M108PKZcWxM7KIULqn9w_wArVQt8F1zgGW1FN4RvJv4) indican que los pacientes han mostrado anomalías de coagulación en la sangre obstaculizando los alvéolos. También existen casos de fallos en otros órganos que contienen las enzimas ACE2 incluyendo cerebro y riñones. 

No es posible predecir si este fenómeno ocurrirá o no en un paciente. Ejemplos existe en el caso de gripe aviar (H5N1) de donde personas han fallecido por fallas de los órganos.  

Por razones aún no conocidas, la estadística indica que los menores  son menos propensos a síntomas graves. [[Kids and COVID-19: What Parents Should Know](https://medicine.yale.edu/news-article/22996/)] Aun así, no se debe de descuidar a los niños y jóvenes pues ha habido reportes de complejidad en pacientes de menor edad. 

Hay jóvenes hoy día con Oxigenación por membrana extracorpórea (OMEC) Extracorporeal Membrane Oxygenation (ECMO). La identificación y tratamiento temprana (early) ha evitado fallecimiento de jóvenes for COVID19.

El [índice de letalidad](https://www.fundeu.es/recomendacion/tasa-de-mortalidad-y-tasa-de-letalidad-diferencia/) depende de factores del entorno: los aparatos médicos disponibles y los hábitos y características de la población. Por ejemplo, en Italia la edad promedio es relativamente alta, por ende un índice de letalidad más alto: 8~9% 

([CDC mortality rate](https://www.cdc.gov/nchs/nvss/vsrr/covid19/index.htm), [worldometer](https://www.worldometers.info/coronavirus/coronavirus-age-sex-demographics/)) {información con grano de sal, pruebas}


> Las personas de la tercera edad, mayor de 80 años, debido al proceso de deterioro del sistema inmunológico por el envejecimiento (conocido como inmunosenescencia), tienen más riesgo de complicaciones al contraer el COVID-19.


Por lo general las personas se recuperan del COVID-19 después de 2 semanas de manera natural produciendo anticuerpos.


## ¿Qué podemos hacer para protegernos?

Lo mejor que podemos hacer es la prevención:



1. Mantenga el distanciamiento social - (ya que puede existir personas transmisoras sin síntomas). La distancia mínima sugerida es de 2 metros (6 pies) entre personas.
2. Evite tocar los ojos, la nariz y la boca con las manos
3. Lave frecuentemente sus manos con jabón o desinfectante (una solución de alcohol 60% mínimo funciona). Cuando se lava la mano con la jabón, frote toda la superficie con jabón por lo menos 20 segundos. Las moléculas del jabón que son similares a la membrana, desestabiliza la capa lipídica del virus desactivándola.

Hay pruebas de que las máscaras y lentes protectores reducen significativamente la posibilidad de contagio.

Las máscaras que bloquean partículas de 5 micrones evitan la entrada de gotitas respiratorias infectadas. Las mascarillas con tela común funcionan, pero no son tan efectivas como las mascarillas de calidad N95, KF94.

Si usted ha estado en contacto directo con una persona contagiada, se recomienda cambiarse las ropas inmediatamente al entrar a la casa.


## ¿Se puede re-contagiar?

En términos clínicos se hace la distinción entre Recontagio (Reinfection) y Recaída (Relapse).

El recontagio se da cuando el sistema inmunológico de una persona ha erradicado el virus pero después de un tiempo se infecta nuevamente.

La Recaída (Relapse) se da cuando después de un tratamiento, el kit de prueba muestra negativo, pero tiempo después una nueva prueba indica positivo nuevamente. Una posible explicación de la recaída es la disminución temporal de la cantidad de virus en el cuerpo por debajo del límite que permite el kit a identificar correctamente la presencia del virus.


> Kits de prueba llamado PCR (Reacción en Cadena de la Polimerasa) se basa en ampliación de secuencia genética del virus para su detección. Se necesita aproximadamente 3K copias de virus en la muestra para que la prueba salga correctamente positivo.


El mecanismo no se sabe a ciencia cierta. Una posibilidad es que el sistema de inmunidad de tales persona no tuvo tiempo de producido el anticuerpo  pero medicamentos como Kaletra o cloroquina (Chloroquine) pudo haber inhibido (suppress) la reproducción del virus. Una vez que dicho paciente termina los medicamentos, el virus cobró su actividad.

La consumación de esteroides puede debilitar la inmunidad y incrementar el virus también.

[diagrama]

Prueba en los monos, anticuerpo que evita reinfección. (Fauci)

La cultura de funeral también puede aportar a la difusión (Spread) del virus

Cremación después de fallecimiento por virus.

En teoría la posibilidad de recontagio existe por lo que aun después de haberse curado, es buena idea seguir tomando precaución.


## ¿Cómo difiere el nuevo coronavirus de las anteriores?

En el pasado hubo epidemias por otros tipos de coronavirus. El SARS-CoV y el MERS-CoV son ejemplos de dichos casos. A diferencia de ellos el nuevo SARS-CoV-2 tiene una tasa de infección más alta, pero a la vez una tasa de letalidad más baja.

Esto se debe a que los receptores de SARS-CoV y MERS-Cov se encuentran en la parte más  profunda de los pulmones.

[diagrama]

La infection rate de MERS-CoV is bajo, ellos se adhieren al receptor DPP4, y el DPP4 se encuentra en la parte más baja del pulmón. So los receptores necesarios para el infección de virus se encuentra mas en la parte superior de la vía respiratoria, la infección es más alta.

La tasa de letalidad de COVID-19 difiere entre países, pero es aproximadamente 2 ~ 4%, mientras que la de MERS es 35%, la de H5N1 (gripe aviar) es 50~60% y la de Ébola es 70~90%.


> Por lo general un tipo de virus no tiene ambos índices altos a la vez.


La otra diferencia es que personas con COVID-19 empiezan a transmitir el virus desde dos días antes de mostrar síntomas.

El SARS y MERS se transmite desde cuando la persona empieza sentir los síntomas, lo cual facilitó identificación inmediata y pronta contención del virus por cuarentena.


## ¿Existe Cura o Vacuna?

No existe vacuna para un nuevo virus recién identificado. Cabe destacar que apenas tenemos 4 meses de haber obtenido la secuencia genética.


> Las vacunas estimulan nuestro cuerpo a produce anticuerpos mediante antígenos que no causan enfermedad, ej. Virus debilitado.


Generalmente el desarrollo de una vacuna para un nuevo virus toma entre 6 a 10 años y necesita varios cientos de millones de dólares. 

En este caso los investigadores y reguladores están trabajando para reducir el tiempo necesario. Si todo marcha de acuerdo a la planificación, se estima un mínimo de 18 meses para que se desarrolle una vacuna. 

Ciertas instituciones alrededor del mundo ya están probando vacunas en los animales y los resultados son prometedores.

Por mientras, existen otras opciones de tratamiento. 

Una opción es la readaptación de medicamentos existentes (drug repurposing). Esta opción, de ser exitosa, puede ser la más rápida en su aplicación, pues como ya están verificados y se conoce sus propiedades, no es necesaria la aprobación por los reguladores.

Entre las medicinas en estudios están los medicamentos para el SIDA, malaria, y ébola (from Gilead).

Un ejemplo es el Chloroquine (hydroxychloroquine) el cual fue mencionado por el presidente de los EE.UU.  El Chloroquine es un tratamiento para la malaria que recientemente se probó contra COVID-19 en Francia. Se observó mejoría en los pacientes, pero aún está en etapa de prueba clínica.

La teoría es que los virus entran al cuerpo más fácilmente en un entorno ligeramente ácido. Los facogitos atacan a los virus, y cuando devoran los virus, generan lisosomas que produce entorno alcalinos, inhibiendo la actividad del virus.

La otra opción es [plasma convaleciente](https://www.bbc.com/mundo/noticias-52129984) (convalescent plasma) el cual consiste en transfusiones de plasma de sangres con anticuerpos de personas que se han recuperado de COVID-19. La plasma con anticuerpos puede ameliorar la situación de pacientes críticas y acelerar su recuperación.

Todos estos trabajos de investigación y desarrollo requieren la inversión de mucho fondos, recursos y tiempo.

En la mayoría de casos en donde la persona no sufre de enfermedades crónicas, los pacientes de COVID-19 se recuperan de manera natural después de 2 semanas. 

Para los casos críticos, los doctores están tomando mucha precaución al utilizar medicamentos, pues ellos pueden causar efectos secundarios indeseables. 


> El efecto secundario de Chloroquine es toxicidad que afecta la visión.


Aunque el desarrollo de la vacuna y medicamentos toma tiempo, lo que sí se puede desarrollar mas rápido son los kits de prueba llamado PCR. El PCR (Reacción en Cadena de la Polimerasa por sus siglas en Inglés) se basa en la ampliación de la secuencia genética del virus para su detección.

Una vez hecha la prueba, el resultado puede obtenerse en aproximadamente 6 horas.

Las pruebas permite intervención más rápida evitando complicaciones en etapas avanzadas.


## ¿Cual es la proyección del COVID-19 en el futuro?

En el 2003 hubo el brote de SARS, en el 2009 el brote de gripe porcina (influenza porcina), [2014 ébola], en el 2015 el brote de MERS, y ahora a finales de 2019 el COVID-19. Los periodos fueron de 6 y 5 años entre brotes.  El COVID19 llegó en un periodo más corto y con impacto más grande.

Basados en datos similares  anteriores, existen tres posibles escenarios de que se puede esperar para el futuro

El primero es el estacional. La enfermedad se mantiene en el hemisferio norte durante las épocas frías de la mitad del año, y la otra mitad, el virus se traslada al hemisferio sur cuando le corresponde el invierno, y el ciclo se repite como lo ha hecho la gripe, pero con mayor impacto.

El otro escenario es que se contenga y desaparezca después de un tiempo. Si sigue el mismo patrón de SARS que apareció en 2002[^1] y terminó en 9 meses, COVID-19 podria terminar entre Julio y Agosto.


> SARS 2002  8 mil infectados, 770 fallecidos (índice de letalidad 9.6%)


El otro escenario optimista es el desarrollo de una vacuna y erradicar el virus por completo.

Al momento de esta grabación la curva global de nuevos casos está aplanando, y en varios territorios empezando a descender. Todos tenemos que aportar para mejorar el descenso de los casos. 


## Informaciones con cautela

En el internet existen informaciones cuales aún no se han confirmado con rigor científico. También hay te tener precaución con otras informaciones que no son veraz y que pueden resultar peligrosas:


1. Aún no se a confirmado que el tipo de sangre tenga relación directa con la probabilidad de contagio y severidad de síntomas.
2. La inyección de desinfectantes, así como el uso de luz ultravioleta directamente al cuerpo es peligroso.
3. Exponerse al calor o al frío intenso no evita el contagio,
4. Tomar alcohol no evita el contagio,
5. Vacunas contra neumonía no protege contra el nuevo coronavirus,
6. Los antibióticos no tiene efecto contra virus, virus tecnicamente no estan “vivos”

(Infopandemic/infodemic: sitios de tesis prematuramente sin comprobación previa)


## Concluyendo

Los médicos y científicos se encuentran en una intensa lucha para lograr un tratamiento efectivo para contrarrestar el COVID 19, trabajando diariamente para salvar la mayor cantidad de vidas de los habitantes en todo el mundo.  Cada dia investigan, aprenden y aplican las experiencias relacionadas con este nuevo virus que ha dejado vulnerable los cimientos de la raza humana. 

Por nuestra parte, debemos continuar de forma vehemente, la aplicación de todas las medidas en pro de la disminución de los infectados, y ser entes multiplicadores de la información oportuna tendiente a salir victoriosos contra lo que constituye la guerra del COVID-19 y nuestro mundo de hoy. 

He allí la importancia de este espacio, el cual debe exhortarnos a otorgar nuestro grano de arena, en pro de lograr la conservación y mejoramiento de nuestras vidas ante esta pandemia que nos estremece.

Mantengase sanos, y sigamos empoderandonos Mas!

Las Informaciones siguen cambiando cada dia. 

Manténgase actualizado.


## Referencias

1. [1ra Entrevista con el Prof.. Woo-Joo Kim de Facultad de medicina en la Universidad de Corea](https://www.youtube.com/watch?v=xafPqcy3lwk). Asian Boss Espanol, Canal YouTube. 
2. [2da Entrevista con el Prof.. Woo-Joo Kim de Facultad de medicina en la Universidad de Corea](https://www.youtube.com/watch?v=GQAcL8z8R3I). Asian Boss Espanol, Canal YouTube. 
3. [A rampage through the body](https://science.sciencemag.org/content/368/6489/356.full?fbclid=IwAR2O0dlXMQMfyPQ-M108PKZcWxM7KIULqn9w_wArVQt8F1zgGW1FN4RvJv4), ScienceMag.org, 24 de Abril 2020
4. [COVID-19 Dashboard](https://coronavirus.jhu.edu/map.html), Center for Systems Science and Engineering (CSSE) at Johns Hopkins University
5. [COVID-19 global impact: How the coronavirus is affecting the world](https://www.medicalnewstoday.com/articles/covid-19-global-impact-how-the-coronavirus-is-affecting-the-world), Medical News Today, 24 de Abril, 2020 
6. [Coronavirus COVID-19 (SARS-CoV-2)](https://www.hopkinsguides.com/hopkins/view/Johns_Hopkins_ABX_Guide/540747/all/Coronavirus_COVID_19__SARS_CoV_2_), Johns Hopkins ABX Guide, April 21, 2020
7. [Enfermedad del coronavirus 2019 (COVID-19), Síntomas](https://espanol.cdc.gov/coronavirus/2019-ncov/symptoms-testing/symptoms.html), Sitio de Centros par el Control y la Prevención de Enfermedades (CDC)
8. [Human Coronavirus Types](https://www.cdc.gov/coronavirus/types.html), Sitio de Centros par el Control y la Prevención de Enfermedades (CDC)
9. [Kids and COVID-19: What Parents Should Know](https://medicine.yale.edu/news-article/22996/), Yale School of Medicine, March 30, 2020
10. [Lo que necesita saber sobre la enfermedad del coronavirus 2019 (COVID-19)](https://www.cdc.gov/coronavirus/2019-ncov/downloads/2019-ncov-factsheet-sp.pdf), Sitio de Centros par el Control y la Prevención de Enfermedades (CDC)
11. [The coronavirus sneaks into cells through a key receptor. Could targeting it lead to a treatment?](https://www.statnews.com/2020/04/10/coronavirus-ace-2-receptor/), Stat News, Abril 10, 2020
12. [WHO Timeline - COVID-19](https://www.who.int/news-room/detail/08-04-2020-who-timeline---covid-19), Organización Mundial de la Salud (who.int), 8 April 2020
13. [Asymptomatic Transmission, the Achilles’ Heel of Current Strategies to Control Covid-19](https://www.nejm.org/doi/full/10.1056/NEJMe2009758?query=RP), The New England Journal of Medicine, Abril 24, 2020
14. [Coronavirus Disease (COVID-19), Statistics and Research](https://ourworldindata.org/coronavirus), Our World in Data
15. [Cytokine storm](https://www.newscientist.com/term/cytokine-storm/), New Scientist
16. https://www.cdc.gov/coronavirus/2019-ncov/faq.html
17. [https://www.cdc.gov/coronavirus/2019-ncov/downloads/2019-ncov-factsheet-sp.pdf](https://www.cdc.gov/coronavirus/2019-ncov/downloads/2019-ncov-factsheet-sp.pdf)
18. [Por qué Covid-19 en algunas personas es asintomático, mientras que en otras letal](https://www.infosalus.com/salud-investigacion/noticia-covid-19-algunas-personas-asintomatico-mientras-otras-letal-20200423083543.html), Infosalus
19. [Coronavirus disease (COVID-19) advice for the public: Myth busters](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public/myth-busters), WHO
20. [The race for coronavirus vaccines: a graphical guide](https://www.nature.com/articles/d41586-020-01221-y), Nature
21. [Can You Be Re-Infected After Recovering From Coronavirus? Here's What We Know About COVID-19 Immunity](https://time.com/5810454/coronavirus-immunity-reinfection/), TIME, Abril 13, 2020
22. [https://www.washingtonpost.com/health/2020/03/23/coronavirus-isnt-alive-thats-why-its-so-hard-kill/](https://www.washingtonpost.com/health/2020/03/23/coronavirus-isnt-alive-thats-why-its-so-hard-kill/)
23. [https://www.cnn.com/2020/04/23/health/coronavirus-herd-immunity-explainer-wellness-scn-trnd/index.html](https://www.cnn.com/2020/04/23/health/coronavirus-herd-immunity-explainer-wellness-scn-trnd/index.html)

<!-- Footnotes themselves at the bottom. -->
## Notas

[^1]:
    Duracion del brote de SARS Noviembre de 2002 hasta julio de 2003
