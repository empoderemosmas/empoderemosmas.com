---
title: Adaptando a la situación actual

author: Young-Suk Ahn Park
date: 2020-04-23
tags: ["post"]
keywords: [ "general", "tendencias"]

videoUrl: https://www.youtube.com/embed/1UwYVQ9p_-0
---

Bienvenidos a la primera sesión de #EmpoderemosMás! Soy el ingeniero Ahn, y en este canal compartiré temas útiles que nos ayudará a elevar nuestro nivel de vida.

En esta sesión, compartiré una perspectiva de la situación actual para que tengamos idea de hacia dónde vamos, y poder identificar factores críticos no solo para subsistir sino _prosperar_.

Hoy día estamos viviendo un tiempo de cambios muy acelerados. Lo en década anterior tomaba años en cambiar, hoy día toma meses, sino semanas. En mi campo, que es ingeniería de software, hace solamente cinco años atrás, la expectativa era introducir nueva funcionalidad en el software en el término de meses. Hoy día los equipos de desarrolladores en empresas de tecnología introducen mejoras al sistema _cada día_. 

Esta rapidez de ciclo no solamente está ocurriendo en el área de informática sino también en otras áreas: por ejemplo en la industria manufacturera se está aprovechando la impresión 3D para explorar productos nuevos, en biotecnología están utilizando técnicas CRISPR para ingeniería genética permitiendo alterar secuencias genéticas con facilidad y precisión.

Estos cambios agigantados está aportando a llevar al límite múltiples sistemas: económico, social, político, educativo, salud y [monetario](https://venturebeat.com/2020/04/18/a-digital-dollar-why-how-and-why-now/), tal como lo conocemos hoy día. Y resulta ser que estos sistemas en que confiamos y dependemos cada minuto es frágil. 

La situación de #COVID-19 que estamos viviendo hoy ha dejado al descubierto las deficiencias de estos sistemas y ha exacerbado los síntomas de sus defectos. Lo más crítico es que hemos puesto el sistema más grande, fundamental e irremplazable en peligro: el _sistema ecológico_.

Todos estos sistemas están entrelazados de manera muy compleja, en donde la causa se convierte a la vez en efecto y la consecuencias se reverbera de tal manera que es casi imposible de predecir.  De igual manera, hoy día todas las entidades e individuos forman una gran red interconectada. Una ruptura en esta red, que a simple vista parece resistente, puede traer repercusiones para las que no estamos preparados.

Para entender esta complejidad en los sistemas, recurrimos al Internet - Google, Facebook, YouTube e innumerables otros sitios.  Si en el pasado la desinformación era un problema, hoy sufrimos de información superflua. La gente no sólo desperdicia tiempo en información de poco valor, sino que está en peligro de aceptar datos falsos como si fuesen verdaderos, lo que puede causar desde simple pérdida de tiempo hasta pérdida monetaria, disturbios sociales y daños a vidas inocentes.

El otro gran reto global que seguimos postergando es el desequilibrio ecológico. La emisión de gases de efecto invernadero está agravando el cambio climático, produciendo anomalías en el medioambiente.  Ahora estamos rindiendo cuenta por el maltrato a la naturaleza. Por ejemplo, estudios indica que la situación actual del COVID-19 guarda relación con la [destrucción del hábitat natural](https://www.bloomberg.com/news/articles/2020-04-08/want-to-stop-the-next-pandemic-start-protecting-wildlife-habitats) ([en español](https://www.tekcrispy.com/2020/04/16/humanos-verdaderos-culpables-derrames-zoonoticos/)). 

Virus contenidos en cierta especie fueron transmitidos a humanos debido a la disminución de sus depredadores naturales, lo que promovió el crecimiento de la población de la especie transmisora y su contacto con los seres humanos. Claro está, décadas de polución - que también contribuye cambio climático - está acomplejando las síntomas de Síndrome Respiratorio Agudo.

## Tendencias primarias
En resumen, estas son las cinco tendencias primarias que nos seguirán impactando profundamente:

1. **Los avances acelerados en la ciencia y tecnología**
    - La bioingeniería, robótica, Inteligencia artificial, etc. puede cambiar por completo la dinámica de la industria y por ende el campo laboral. Para ello, tenemos que actualizarnos constantemente para no quedarnos obsoleto.
    - Nos afecta la manera como percibimos e interactuamos con el mundo, el estilo  y patrón de vida.
    - Un impacto negativo se manifiesta como incertidumbre laboral.
2. **Segundo, El acercamiento al límite de los sistemas, que de por sí ya son muy complejos.**
    - Esta complejidad está lleno de burocracias innecesarias reduciendo la eficiencia y capacidad de los ciudadanos y entidades. Tenemos que reimaginar la implementación de estos sistemas o cambiar paradigmas. Utilizando parabola, tenemos que reemplazar vino agrio con el nuevo vino.
    - Nos afecta en la eficacia al ejercer funciones esenciales y cívicas.
    - Se aplica tanto a sistemas creados por humanos como a sistemas naturales.
    - [[Cynefin](https://en.wikipedia.org/wiki/Cynefin_framework)]
    - Un impacto negativo se manifiesta como crisis que pueden durar desde meses hasta años. Por ejemplo, el colapso económico de 2008. Inestabilidad socio-económica, política, etc.
3. **Tercero, El incremento de la conectividad y enlaces entre entidades e individuos.**
    - Esta conectividad permite fluidez de información, bienes y personas. Nosotros tenemos que tomar ventaja de este mundo conectado, donde las fronteras ya no son obstáculos, Tenemos que ampliar nuestros horizontes, y materializar soluciones de mayor escala.
    - Nos afecta cuando intercambiamos información. Y cuando nos trasladamos de un lugar a otro.
    - Se aplica en comunicación digital desde dispositivos (IoT) hasta servidores y transporte físico.
    - Un impacto negativo se manifiesta como transmisión de virus digital y biológico, catástrofe económica en cadena. (un cambio en una parte afecta de manera inesperada en otra parte.)
4. **Cuarto, la sobreabundancia de información**
    - Con un solo click podemos obtener enorme cantidad de información. Son tanta la información que tenemos que saber discernir los datos útiles y correctos de los falsas o de poco valor. 
    - Nos afecta cuando consumimos información.
    - Se aplica a la información pública y privada.
    - Un impacto negativo se manifiesta como pérdida de activos o recursos debido a información engañosa o falsa información. 
5. **Y por último quinto, el acercamiento al punto crítico de quebrantamiento del balance natural.**
    - La contaminación del ambiente y el abuso de la naturaleza está afectando la salud física y mental, y en el nivel más alto, el equilibrio global. Es primordial proteger el planeta tierra, para nosotros y las próximas generaciones, porque es nuestro único hogar.
    - Nos afecta en la necesidad más esencial: aire, agua, comida… (el nivel más bajo en la pirámide de Maslow)
    - Se aplica en lo fundamental de vida.
    - Un impacto negativo se manifiesta como cambio climático, patógenos zoonóticos, nutrición deficiente, enfermedades nuevas, reducción de tierras fértiles, inestabilidad social

Aunque la situación parezca depresiva y abrumadora, la buena noticia es que tenemos al alcance todas las herramientas necesarias para solucionar. Solo nos hace falta facultarnos y tomar acción.

Todo esto nos trae de vuelta a lo fundamental: tener una mentalidad fuerte, ser ágil, creativo,  resiliente, resistente, adaptar y movernos hacia la ejecución, de manera positiva. 

El objetivo de este canal es el #CrecimientoPersonal y #Empoderamiento, para desarrollarnos profesionalmente y mejorar nuestro entorno, incluyendo la sociedad.

Es el momento de cuestionar, preguntar el por qué, comprender los motivos y las razones, identificar fallas y limitaciones, y crear un nuevo mercado con soluciones creativas y sostenibles. Ahora es el momento de empoderar y actuar.

Empoderemos Más!  
