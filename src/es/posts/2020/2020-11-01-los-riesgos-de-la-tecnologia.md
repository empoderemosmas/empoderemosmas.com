---
title: ¿Está usted preparado para los riesgos de la tecnología?

author: Young-Suk Ahn Park
date: 2020-11-01
tags: ["post"]
keywords: ["tecnologia"]

videoUrl: https://www.youtube.com/embed/EpPllAtgvFo
---

La fascinación y comodidad que nos ofrece la tecnología es seductora. Si nos dejamos llevar por ella sin tener buen criterio, podemos caer en la ilusión de mayor productividad y abundancia, perdiendo de vista nuestros propios objetivos y, sin darnos cuenta, cediendo el control de nuestras vidas.

Continuando con el tema sobre los peligros de los avances digitales, en esta sesión veremos los riesgos subliminales que son más difíciles de percibir, aquellos que le ponemos poca atención hasta que se han materializado y el remedio es muy costoso.

## Manipulación social
Tal es el caso de la manipulación social. Las personas son atraídas por la prensa amarillista, las noticias sensacionalistas. Estas noticias generan dopamina, hormona responsable de la sensación de placer, motivación entre otros, incitando la curiosidad y dejándonos enganchados.Tomando en cuenta los antecedentes humanos, estos tipos de noticias solían servir como señal para la supervivencia, por lo que los humanos se han vuelto sensitivos a tales informaciones. Hoy día es utilizado como gancho por organizaciones para atraer atención.

Grupos radicales explotan esta naturaleza humana, logrando viralidad en plataformas digitales e influenciando a las masas. Estos grupos utilizan de manera efectiva las redes sociales difundiendo propagandas tóxicas y consiguiendo reclutar seguidores, recaudar fondos y movilizar simpatizantes para su beneficio, inclusive hasta dañando a aquellos que no comparten los mismos ideales.

Cuando la narrativa es parcial, intolerante, denigrante, favoreciendo solamente un pequeño grupo empieza a brotar en la sociedad la discriminacion y el racismo resultando en la fragmentación social.

Las implicaciones de las plataformas sociales son tan complejas que las leyes aún no proveen reglamentos efectivos para identificar y anular estas actividades dañinas.

Lo que debemos hacer nosotros es no aceptar cualquier información que pueda tener repercusiones mayores, sin haber investigado más a fondo. Tengamos criterio propio y entendamos que está bien llevar la contraria si así lo indica nuestros valores y principios.

## Riesgos intrínsecos

Existen otros riesgos que son intrínsecos que vienen del mal uso o el abuso de las tecnologías. Esto afecta la salud mental y el funcionamiento efectivo de la cognición.

¿Cuántas veces hemos revisado el chat cuando estamos manejando? Este simple acto puede resultar trágico. El Consejo Nacional de Seguridad de los EE. UU. estima que el 26% de los accidentes son causados por distracción al usar los celulares [link](https://www.nsc.org/in-the-newsroom/public-ready-for-stiffer-penalties-for-texting-while-driving). 

Otros efectos negativos subliminales de la tecnología tenemos:
1. La adicción: La adicción a los juegos de video es ahora una enfermedad clasificada por la Organización Mundial De La Salud (OMS) bajo el nombre del “trastorno del juego”. 
> Para ser diagnosticado como trastorno del juego, la persona debe de presentar deterioro significativo en el funcionamiento personal, familiar, social, educacional y ocupacional. 
Ya se han reportado violencia familiar en donde jóvenes han agredido a sus padres al prohibir los videojuegos. También hubo casos en donde los padres de recién nacido han dejado a su niño padecer hambre por estar concentrado en videojuegos.
Las redes sociales también pueden volverse adictivas, afectando negativamente el desenvolvimiento social y la salud emocional.

2. Impacto psicológico y emocional. Cada vez pasamos más tiempo con dispositivos, y menos tiempo interactuando directamente con otros humanos. Esto impide la práctica y desarrollo de facultades importantes como empatía, perseverancia, resolución de conflictos y otras habilidades sociales; contribuyendo a la ampliación del abismo social. Las personas tienden a buscar lo instantáneo, gratificación con mucho estímulo pero sin necesidad de esfuerzo.
Por otra parte, las redes sociales nos prometen “un mundo más conectado y social”, desafortunadamente también han traído depresión debido a que inducen a las personas a compararse con otros. Al ver el traje extravagante que recién compró tu amiga, o la noticia sobre la promoción en el trabajo de tu viejo amigo, o el viaje en crucero del vecino, activa nuestra voz interna negativa que alimenta la envidia llevándonos a la auto frustración. 
3. Disminución de habilidades cognitivas. El otro problema es que nos hemos vuelto muy dependientes de los dispositivos digitales. Muchas de las funciones que ejercía uno mismo han sido externalizadas a dispositivos. ¿Cuántos números de teléfono memorizamos hoy? Yo apenas me pude memorizar el mío y el de mi esposa. Y ¿qué hay de cálculo mental, o recordatorio de eventos, o información de personas?  La mente, como cualquier otra parte del cuerpo humano, con el desuso va decayendo su destreza. Tenemos el riesgo de caer en la subordinación a la tecnología para mantenernos “productivos y una vida normal”, dejándonos en ciclo vicioso, lo cual además de debilitar nuestras facultades mentales, incrementa la vulnerabilidad de ser vigilado y manipulado por entidades externas.

Para contrarrestar estos efectos negativos,
1. Primeramente tenemos que tener claro nuestros valores, las creencias fundamentales que nos guían.
2. Segundo, tener el hábito de auto-evaluación, teniendo como objetivo una vida consciente y balanceada, alineada con nuestros valores.
3. Tercero, mantener una interacción sana con personas, aprendiendo a aceptar y manejar las diferentes emociones.
4. Y cuarto, utilizar las diferentes facultades cognitivas, ejercitando la mente y manteniéndola saludable.

## Temas éticos
La tecnología digital también está produciendo repercusiones negativas en el ámbito de la ética. 

Si piensan que las computadoras producen decisiones más objetivas, imparciales, sin favoritismo, están equivocados.
La inteligencia artificial, que es utilizada para realizar las decisiones, se basa en la identificación de patrones, y la manera cómo funciona es por entrenamiento mediante ingreso masivo de datos produciendo un modelo de clasificación. Si los datos entrenados son favorables a un grupo minoritario, entonces la evaluación saldrá desfavorable a los demás grupos. Estas discriminaciones no son fáciles de detectar, y aún si las detectamos, ¿quién es el responsable?, ¿el desarrollador, el operador, la empresa?

Para muchos, ya no es exageración decir que los algoritmos están decidiendo el lugar donde vivimos, la universidad donde asistimos, el lugar donde trabajamos. Las empresas utilizan algoritmos y datos para decidir qué productos promover a diferentes grupos de audiencia. Las entidades financieras utilizan inteligencia artificial para aprobar o rechazar la solicitud de préstamos e hipotecas. Organizaciones están utilizando aprendizaje automático para filtrar candidatos de trabajos. Posiblemente las compras que usted realiza están influenciado por la publicidad estratégicamente dirigidas por las plataformas inteligentes. Todo esto puede exacerbar la brecha de la riqueza. 

Recientemente Google ha publicado su plan de lanzar servicio de ética sobre inteligencia artificial, por el cual proveerá consejos sobre identificación de prejuicio racial en proyectos relacionados con IA [WIRED](https://www.wired.com/story/google-help-others-tricky-ethics-ai/). Pero poco sabemos sobre los posibles conflictos de intereses.

Por otro lado, las plataformas sociales han introducido nuevas dimensiones en la dinámica social. Las redes sociales son incubadoras de nuevos dilemas sociales y políticos.  Desde las  imágenes y noticias aberrantes que trastorna la juventud, hasta la interferencia en el proceso democratico de un país.
Tengan en cuenta que las redes sociales son plataformas operadas por empresas con fines de lucro, las ganancias vienen por la información de cada persona y del tiempo que ellos pasan apegados a sus plataformas. Piensen en la vulnerabilidad relacionada con la violación a la privacidad.


> Riesgos éticos:
> 1. Parcialidad de algoritmos e inteligencia artificial 
>   - Ejemplo: analisis de credito, 
>   - Accidentes por carros automaticos
> 2. Mayor polarización, aumento en la brecha económica
>   - Mayor privilegio a los que ya están privilegiados.
> 3. Violación a la privacidad.
>   - Rastreo de información privada
>   - Sobre vigilancia de ciudadanos por gobiernos u otras entidades.

Una vez más cabe reiterar la importancia de estar consciente, y saber defender sus derechos y respetar los derechos de otros. También participar en iniciativas de equitatividad digital y fomentar diálogos en entornos de diversidad e inclusividad.

## Desplazamiento laboral por inteligencia artificial.
El último en esta lista de riesgos es el desplazamiento laboral por automatización. En 1997 la supercomputadora derrotó por primera vez a un gran máster de ajedrez, Garry Kasparov. El ajedrez es un juego de estrategia que se consideraba neces ario alto grado de intelecto. Diez años después, en 2016, el AlphaGo derrota al campeón del juego Go. El go es un juego de estrategia que por su posible permutación (10700), era considerado computacionalmente imposible que hasta una supercomputadora pueda ganar al campeón humano.

Así mismo vemos que el diagnóstico de enfermedad, detección de cáncer, análisis de documentos legales, recomendación financiera, hasta creación de arte y composición de música, que son áreas de especialidad que anteriormente pensamos que las computadoras jamás podría reemplazar humanos, ahora es realidad.

Con el paso que vamos, en un par de años habrá innumerables nuevos servicios automatizados reduciendo la necesidad de ciertas labores humanas. En cinco o diez años habrá cambios en los paradigmas que dejará obsoletos muchos de las habilidades y conocimientos actuales.

## Concluyendo
La rápida adopción de la tecnología digital es acelerada aún más por la pandemia de COVID-19. Los cambios ocurren sin esperar que nosotros, la sociedad, tenga suficiente tiempo para prepararse y adaptarse. 

Las personas tienden a enfocarse demasiado en la productividad y la satisfacción instantánea, sin dar debida importancia a las cosas que realmente impactan la calidad de nuestra vida.

Tenemos que diferenciarnos de la automatización, fortaleciendo nuestro discernimiento y  nutriendo nuestra parte creativa y humana. 

En este tiempo en donde las computadoras están tomando más porción de las habilidades de la mente, es cuando más necesitamos estar despiertos y conscientes, y potenciar la facultad humana.

En lengua asiática, la palabra riesgo es una combinación de dos palabras: peligro y oportunidad. Es muy cierto que cada uno de estos riesgos trae consigo muchas otras oportunidades. 

Ahora que hemos recorrido los riesgos de las tecnologías digitales, enfoquémonos en las oportunidades y empecemos a explorarlas!

## Referencias
- [Video Game Addiction Is Now a Real Disease (And What Help Is Available)
](https://health.clevelandclinic.org/video-game-addiction-is-now-a-real-disease-and-what-help-is-available/), Cleveland Clinic
- [El Trastorno del Juego, una enfermedad que podemos identificar y prevenir](https://www.enticconfio.gov.co/El_Trastorno_del_Juego_una_enfermedad_que_podemos_identificar_y_prevenir)
- [The Future of Well Being in a Tech-Saturated World, Pew Research Center](https://www.pewresearch.org/internet/2018/04/17/concerns-about-the-future-of-peoples-well-being), PWE
- [Digital Media and Society, Risks](https://reports.weforum.org/human-implications-of-digital-media-2016/downsides-and-risks/), World Economic Forum
- [The 7 most dangerous digital technology trends](https://www.helpnetsecurity.com/2019/12/10/dangerous-digital-technology-trends/), HelpNetSecurity.com
- [Use seguro y responsable de las TIC](http://www3.gobiernodecanarias.org/medusa/ecoescuela/seguridad/riesgos-asociados-al-uso-de-las-tecnologias/riesgos/)
- [How AI-generated music is changing the way hits are made](https://www.theverge.com/2018/8/31/17777008/artificial-intelligence-taryn-southern-amper-music)
