---
title: Los peligros perceptibles en la sociedad digitalizada

author: Young-Suk Ahn Park
date: 2020-08-09
tags: ["post"]
keywords: ["tecnologia"]

videoUrl: https://www.youtube.com/embed/nQRCsMPtMWo
---


La tecnología digital está permeando en el tejido de la sociedad y está alterando cada aspecto de ella. Los avances tecnológicos nos cautivan, pero por detrás y por debajo los acompaña la parte oscura y negativa. A veces son efectos secundarios dañinos, pero muchas veces son actos maliciosos, aprovechándose de las capacidades de las tecnologías. Estos actos y efectos negativos están quebrantando la estabilidad socioeconómica.

Esto me hace recordar la frase "Un gran poder conlleva una gran responsabilidad" de Stan Lee, o Spider Man (el hombre araña).

Los avances digitales son superpoderes, que en manos de personas sin el debido  conocimiento o con intenciones maliciosas, pueden traer resultados fatales.

Ya conocerán los peligros de naturaleza técnica: los programas maliciosos, como los virus y los ataques por los hackers a computadoras personales, empresariales y hasta sistemas gubernamentales. Estos causan daños que son visibles, cuantificables y con la posibilidad de identificar a los responsables; por lo que es posible tomar medidas concretas para su defensa. Solamente en EE. UU. el costo económico por los ataques cibernéticos en el 2016 fue de entre $57 a $109 billones de dólares.

Por otra parte, existen peligros que son más subliminales. Difíciles de discernir al comienzo pero que permean en la sociedad - comparable a una pandemia. La identificación del responsable o del factor responsable es sumamente difícil, por ende, es un reto defenderse o tomar medidas preventivas efectivas. Estos peligros incluyen la adicción y otros efectos psicológicos, manipulación social, e imparcialidad de los algoritmos en los sistemas.

También hay que tomar en cuenta que cada vez más el mundo digital se extiende al mundo físico, llevando el mundo físico a un estado más frágil. La llamada IoT (Internet of Things) o “Internet de las cosas” conecta dispositivos a internet e interactúa con servicios en las nubes. Esto significa que las cerraduras automáticas de las puertas, accesorios inteligentes (televisores, refrigeradores, microondas, etc), marcadores de latidos, respiradores, entre otros; son vulnerables a los ataques originados desde el plano digital. No quiero imaginar lo que pasaría si los terroristas llegasen a tomar control de las estaciones nucleares!

Examinemos con más detalle los riesgos y peligros tangibles.


## Ataques a propiedades digitales

La primera categoría son los ataques a propiedades digitales, esto incluye los programas maliciosos, y penetraciones a redes - los llamados hackings. Tales ataques generalmente tienen como propósito el hurto, estafa, chantaje, o extorsión para extraer dinero o propiedades de valor de las víctimas.

Existen varios tipos de programas maliciosos:



*   Los [Virus](https://es.wikipedia.org/wiki/Virus_inform%C3%A1tico):  Consisten en softwares que tienen por objetivo alterar el funcionamiento normal de cualquier tipo de dispositivo informático, para lograr fines maliciosos como la destrucción de archivos.
*   Los [Gusanos](https://es.wikipedia.org/wiki/Gusano_inform%C3%A1tico) (worms): son aquellos que se replican para propagarse a otras computadoras, aprovechando las fallas de seguridad en la computadora de destino para acceder a ella.
*   Los  "[secuestros de datos](https://es.wikipedia.org/wiki/Ransomware)" ([Ransomware](https://es.wikipedia.org/wiki/Ransomware)): son programas dañinos que restringen el acceso a determinadas partes o archivos del sistema infectado y luego piden un rescate a cambio de quitar esta restricción.
*   Los [Programas espía](https://es.wikipedia.org/wiki/Programa_esp%C3%ADa) (spyware): estos recopilan información de una computadora y después la transmiten a una entidad externa sin el conocimiento o el consentimiento del usuario.

Los programas maliciosos existieron desde los años 70, y al pasar los años, las herramientas y prácticas de prevención han madurado y mejorado. Pero aún los daños siguen ocurriendo.

Para contrarrestar y prevenir estos programas maliciosos, estas son las recomendaciones



1. Mantenga el sistema operativo actualizado,
2. Escanee su sistema con un antivirus actualizado.
3. Corra el cortafuegos (firewalls), para que los intrusos tengan menos chance de ingresar al sistema.
4. No abra emails sospechosos, e.j. De una persona no conocida o con título sospechoso
5. Habilite un bloqueador de ventanas emergentes en su navegador de internet.
6. Evite visitar sitios inseguros (sin certificado SSL)
7. Nunca descargue aplicaciones de origen desconocido.

La otra manera de ataque es por penetración al sistema. Los hackers disponen de algoritmos para sondear vulnerabilidades e infiltrarse en las computadoras para extraer información como clave de bancos, o utilizarla como un recurso intermediario para otros ataques.

Muchas de las vulnerabilidades son huecos que los desarrolladores del software pasaron por alto. Las penetraciones vienen por conexión al internet. Cada computadora conectada al internet tiene una dirección única y tiene orificios digitales llamados _puertos_ por donde las aplicaciones se conectan con la red.  Es por esos puertos abiertos por donde entran los ataques. Los ataques suelen ocurrir en sitios populares de internet. En julio de 2020, el sitio social twitter.com fue infiltrado por hackers tomando control de cuentas de celebridades como Elon Musk, Barack Obama, Bill Gates, entre otros. Como otro ejemplo tenemos el hacking ocurrido a Sony Studio en Noviembre 2014. El daño fue de $15 M. Las empresas en todas las industrias estan invirtiendo millones de dolares anuales para contrarrestar constantes ataques [[https://en.wikipedia.org/wiki/List_of_data_breaches](https://en.wikipedia.org/wiki/List_of_data_breaches)].

Interesantes son los casos en donde los datos para infiltrar al sistema son obtenidos por interacción social, esta técnica es llamada ingeniería social. También han hecho mucho daño económico y emocional los mails engañosos (los phishings) que estratégicamente dirigen a personas vulnerables como las personas de la tercera edad.

Para contrarrestar estos ataques,



1. No ponga al descubierto información sensible personal tal como teléfono, número de cédula, pasaporte o seguro social, dirección, fecha de nacimiento, etc.
    1. Los asaltantes pueden agregar información para obtener claves.
2. No confíe en los emails que piden su información personal. Por lo general correo electrónico es considerado medio inseguro.
3. Si alguna página le pide que introduzca información personal, primero verifique que tenga conexión segura - que la dirección empiece con https y que muestre el icono de cerradura;, y segundo que la dirección tenga el dominio correcto.
    2. Si supuestamente está conectando con el el banco llamando [Buen Banco, el dominio debe ser buenbanco.com, no buenbanco.bco.com o     .
4. No suba información que pueda inferir que usted se encuentra lejos de su casa por tiempo prolongado.
5. En los sitios web, registrese con contraseña segura, no utilice palabras o números fáciles  de adivinar - como su fecha de nacimiento. Existen programas que utilizan algoritmos con palabras comunes y números para intentar ingresar al sistema.
6. No reutilice la misma contraseña en otros sitios. Cuando un sitio es hackeado, los asaltantes intentan en otros sitios con los mismos credenciales.
7. Si mantiene un sitio web, actualice constantemente el framework y las librerías.
8. Ignore todos esos emails y páginas diciendo que usted tiene suerte y que gano la loteria. Si los abre, posiblemente su suerte sea lo contrario.
9. Si alguien desconocido o recién conocido empieza a preguntar información personal, no  acceda.


## Ataque digital a la reputación (Cyber mob attack)

El otro tipo de ataque digital es el ataque a la reputación y dignidad humana. La red social que es una plataforma que corre encima de la tecnología digital, es también un medio que es utilizado para difamar y dañar la reputación personas. Como casos comunes tenemos las venganzas cibernéticas, también la denigración de oponentes políticos, activistas sociales o grupos y razas específicas.

Estos ataques han existido desde antes por medios tradicionales pero ahora es mucho más fácil producir propaganda y mensajes despectivos de forma anónima en escala masiva.

También cabe mencionar que entre jovenes estan ocurriendo intimidaciones y acosos cibernéticos que están causando depresión y hasta suicidios.

Ahora los agresores cuentan con una nueva técnica - la inteligencia artificial para producir los DeepFakes. Con esta técnica, las personas con malas intenciones están produciendo imágenes y videos sintéticos falsos con escenas ultrajantes utilizando la foto de sus víctimas. El resultado es una difamación con detrimento y heridas irreparables.

Para contrarrestar estos ataques,



1. Nosotro tenemos que conocer las leyes, nuestros derechos y deberes.
2. Tenemos que tener idea de las capacidades de las tecnologías, y sus implicaciones
3. Y también ejercer vida civil adecuada

Lo triste es que en la mayoría de los casos estos ataques son muy difíciles de resguardar y recuperar después del daño ocurrido. Lo importante es concientizar a la sociedad y educar a nuestros hijos e hijas  para que tengan sentido moral y mayor resistencia.


## Otros Cibercrimenes

Además de los ataques, el medio digital es utilizado para lograr otros ciber-crimenes como mercado negro de productos ilicitos tales como armas y estupefacientes; trafico de sexo y fraude. Estos actos corrompen a la sociedad dejando marcas indelebles.

En esta sesión hemos visto cómo los avances digitales pueden servir de medio para causar daños  a la propiedad y a  la reputación de personas.

La precaución y prevención es la mejor medida.

En la próxima sesión tocaremos el tema de riesgos menos perceptibles pero que no dejan de ser dañinos y malignos.

Estemos más alertas y  preparados.


# Referencias

- [Threat Types, Scams, Attacks & Prevention](https://www.phishprotection.com/resources/what-is-phishing/)
- [“How deepfakes undermine truth and threaten democracy”](https://www.ted.com/talks/danielle_citron_how_deepfakes_undermine_truth_and_threaten_democracy), Danielle Citron, TED
- [Why We Need To Democratise How We Build AI](https://www.youtube.com/watch?v=D28aL_5LH2Q), Karen Hao, TEDx
- [Security News This Week: Who Pulled Off the Twitter Hack?](https://www.wired.com/story/twitter-hack-suspect-vpns-securit-news/), Wired, 2020
- [The Cost of Malicious Cyber Activity to the U.S. Economy](https://www.whitehouse.gov/wp-content/uploads/2018/03/The-Cost-of-Malicious-Cyber-Activity-to-the-U.S.-Economy.pdf), The Council of Economic Advisers, an agency within the Executive Office of the President of USA, 2018
