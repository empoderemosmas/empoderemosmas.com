---
title: Retrospectiva 2020, el año que nos cambió profundamente

author: Young-Suk Ahn Park
date: 2020-12-31
tags: ["post"]
keywords: ["retrospectiva", "resumen", "2020"]

videoUrl: https://www.youtube.com/embed/VS_IFm09Sig
---

El 2020 fue un año que nos cambió profundamente.

Este año, los conflictos políticos entre Turquía y Siria, entre Azerbaiyán y Armenia, entre India y China, la guerra civil en Yemen, y la crisis del golfo pérsico estremecieron la política global. Los que sufrieron más fueron los inocentes del pueblo. 

Este año, las actividades terroristas en Nigeria y Afganistan, la ley de seguridad impuesta por China en Hong Kong, la violencia y opresion a las mujeres en diferentes partes del mundo, y la muerte de George Floyd, Breonna Taylor, Jacob Blake y otros ciudadanos de color debido a la brutalidad policial repercutieron intensamente en la sociedad mundial.

**La gente alzó su voz para exigir sus derechos, para reclamar su dignidad.** Y el clamor continúa.

Este año **millones de hectáreas fueron hechas cenizas** por incendios forestales. Centenares de millones de plantas y animales que formaban parte de la invaluable riqueza natural fueron convertidos en polvo .
Cubierto de humo, el cielo se oscureció, y nos hizo visualizar lo que sería nuestro lóbrego futuro.
Una de las herramientas más antiguas del hombre, el fuego, se ha convertido en una señal de advertencia de que la civilización ha ido demasiado lejos.

Este año se batió el **récord de temperatura** más alta en varios puntos del mundo, y nevó en lugares que no había nevado por décadas.
También fue el año más activo de huracanes en el atlántico, y uno de los años con más lluvias torrenciales en partes de Asia y África.
La misma agua que es esencial para la vida, demostró que también destruye y quita vidas.

Con incendios e inundaciones, con calor extremo y frío intenso el planeta nos envía mensajes: “¡Ya basta!”

> Consumo descontrolado de riquezas naturales, derrames de petróleo, contaminación de plásticos y otros desechos tóxicos estan quebrantando el balence ecologico. 

El planeta alzó su voz para exigir respeto, **para que tomemos conciencia de nuestros actos descontrolados**. 

La última advertencia hasta el día de hoy: el COVID-19.
**El COVID-19 disrumpió el mundo como ningún otro evento en la historia humana.** 
Personas de todas las naciones, sin importar el sexo, la raza, o el estado social fueron afectados por el virus.

> Hasta el momento de este artículo se reportó 78 millones de casos de infecciones y 1.7 millones de muertes.

En unos cuantos meses, la pandemia produjo una serie de reacciones en cadena impactando la salud física y mental de la sociedad, transformando las industrias y la economía, cambiando los métodos de la educación, acelerando la ciencia y tecnología. COVID-19 ha alterado la vida de cada uno de nosotros.  Otra vez, **los que más sufrieron fueron los marginados, los débiles**.

Debido a nuestra codicia, nuestra comodidad y nuestra indiferencia **hemos sido voluntariamente ciegos y sordos** ante estas manifestaciones.


El 2020 fue un año de desesperación, pérdida, dolor y caos. Fue el año donde la familia, los médicos, los científicos, los gobiernos, los sistemas establecidos por humanos, en pocas palabras, la humanidad en sí fue puesta a prueba. Nos encogimos, aguantamos, esforzamos, resistimos y maduramos. Abrimos los ojos y **nos dimos cuenta que héroes y ángeles son personas como tu y yo**.

Fue el año donde hemos demostrado que sabemos alzar nuestra voz y luchar ante la injusticia; que somos compasivos ante la tragedia;  que podemos adaptarnos para seguir adelante y que podemos colaborar para lograr metas comunes en tiempo récord.

El 2020 nos dejó cicatrices, pero ellas simbolizan **valiosas lecciones**. Aprendimos que debemos ser *humildes ante la naturaleza*; que *somos vulnerables ante minúsculo virus*; que *todos estamos conectados*; y que *si unimos fuerzas podemos lograr lo que antes pensábamos imposible*.

Lo que **necesitamos poner en práctica ahora es la reconciliación**. Reconciliación con uno mismo, con la familia, con las mujeres y grupos minoritarios, con las naciones en conflicto y más que nada con la naturaleza.

Gracias al  2020 sabemos que cada acción nuestra, grande y pequeña, cuenta.

Seguiremos aprendiendo, innovando y avanzando; y en **el futuro veremos de vuelta al 2020 como el escalón que nos sirvió para tomar cursos correctivos y dirigirnos hacia un mundo mejor**.



> Dedicado a todos los héroes y ángeles que se esforzaron hasta el límite durante la pandemia, y a todos los que fallecieron en 2020, que sus almas gocen de paz eterna.
