---
title: Preguntas importantes para una vida de éxito

author: Young-Suk Ahn Park
date: 2021-01-30
tags: ["post"]
keywords: ["proposit", "resumen", "2020"]

videoUrl: https://www.youtube.com/embed/KtiBHuAUpJ4
---

## Introducción

Llega momentos en la vida que sentimos insatisfechos de nosotros mismos y nos preguntamos “¿Por que mi vida anda asi?” Y reñimos con las gentes, con el mundo y nos decimos “Esfuerzo tanto para ser una buena persona, pero ¿por qué no me llega la fortuna?”

La frustración es más agravante cuando no tenemos respuestas prácticas. 

Si usted no se ha hecho tales preguntas aún, es tiempo para hacer una pausa, evaluar su situación y reflexionar. No vaya ser que usted se esté convenciendo de estar satisfecho de una vida cómoda pero insípida, sin mucho significado.

Y si las preguntas ya son familiares, el hecho de hacer estas preguntas significa que desea algo mejor para su vida, el problema es que tales preguntas pocas veces ayudan, más bien nos deprime. 

Empezando el 2021 reformulemos las preguntas para que nos sean útiles y nos puedan ayudar a calibrar nuestro compás de vida y alinear nuestras acciones.


## Contexto

La ironía de muchos, incluyendome, es que vivimos tratando de satisfacer las expectativas de otros, obedeciendo reglas impuestas sin saber el “¿por qué?”

A nivel psicológico, en el subconsciente nosotros pasamos la mitad de nuestro tiempo tratando de impresionar o complacer a otras personas para obtener su validación y aceptación.

Fijense en las redes sociales. Todos tenemos esa experiencia de subir fotos o publicar algo en Facebook y luego nos apegamos a la computadora esperando que alguien nos deje comentarios de admiración o aprobación. Y que tanto nos emocionamos cuando vemos el conteo de “Me Gusta” seguir subiendo. Cuando el conteo llega a miles, estamos en éxtasis.

A nivel  sociológico, en el proceso llamado **condicionamiento social** los individuos en una sociedad son “entrenados” para que respondan de una manera generalmente aprobada por la sociedad, por los adultos, los políticos, las grandes empresas que mueven las industrias. Y se han preguntado cuáles son los intereses originales? Sin darnos cuenta, nosotros los adultos hemos sido cómplices de la adoctrinación de los jóvenes, pues así nos hicieron a nosotros.

Piensen por un momento. Desde pequeños nos han educado de que tenemos que portarnos bien, que tenemos que obedecer las reglas, que tenemos que obtener buenas notas, para poder ir a buena universidad, para luego poder obtener buen trabajo, para entonces poder ganar buena plata, conseguir una buena pareja, conseguir una casa en una buen vecindad,  para al fin ser feliz.

Ese es el esquema impuesto por la sociedad, y sacrificamos las cosas más valiosas en nuestra vida para ajustarnos al patrón monocromático que adoptamos sin cuestionar.

Y así, al no satisfacer nuestros auténticos objetivos, nos frustramos, y peor aún, pasamos nuestras frustraciones a la próxima generación. Y la historia se repite.


### ¿Qué creemos que es el éxito?

Es una falacia creer que el éxito es sólo cuando una persona disfruta de riqueza o admiración de otros.  Lo cierto es que existen muchas formas de éxitos, más bien, cada persona tiene su propia forma de éxito, pero no todos la realizan.  La otra falsedad que creemos es que el éxito es un estado al que llegamos, un destino. Yo digo que el éxito es la trayectoria que seguimos caminando constantemente.

Cada persona tiene una trayectoria única, diferente de los demás, y tenemos que aceptar la diferencia. Lo que yo considero como éxito es diferente de lo que mi papá o mamá consideraba como éxito. También es diferente de mis hermanos, mis amigos, mis profesores, mi jefe, mis colegas o los montones de personas en Facebook, Twitter, YouTube, TikTok.  Asimismo hay que reconocer que los éxitos de mis hijos son diferentes al mío. Así que dejemos de presionar a los pobres muchachos y muchachas, y celebremos su originalidad!

Conscientes de esto, ¿cómo yo puedo saber si estoy caminando la trayectoria de mi éxito?


> Pero estrecha es la puerta y angosto el camino que conduce a la vida, y son pocos los que la encuentran. Mateo 7:14


## Los ejercicios

Ahora procedamos hacer dos ejercicios para que podamos definir _el_ concepto del éxito de cada uno.  Así podremos ajustar nuestros hábitos y dirigir nuestras acciones.

Para que Los ejercicios resulten útiles, es crítico que usted se libere de la mentalidad restringida sin caer en la trampa del conformismo:



*   Antes que nada, olvídese de las expectativas de su familia. De lo que su papá espera o esperaba de usted. De lo que su esposa desea que usted sea. 
*   Esté consciente del efecto que tuvo el condicionamiento social, de cómo la sociedad nos ha programado nuestro comportamiento y pensamientos. Libérese de las “normas” impuestas por los humanos, y las etiquetas que han puesto las gentes. Que el macho no llora, que las chicas no son buenas en la robótica. Que el papá es el que da sustento económico mientras que la mamá es la que cocina.
*   Deje sus preconcepciones, de que la vida funciona de cierta manera.  A veces uno asume sin nunca confirmar. Le aseguro que aunque se trague semillas de sandía, ninguna sandía va a crecer en su estómago.

Sin contradecir sus principios éticos y morales, permita que estos 15 minutos de ejercicio usted sea genuino.

Preparen dos hojas de papel y una pluma o lápiz.


### El primer ejercicio: preguntas de guía

El primer ejercicio consiste en tres preguntas que servirán de guía. Este ejercicio está basado en el seminario de Vishen Lakhiani de MindValley. Si quieren saber más, pueden ir al enlace debajo. 

Importante:



*   No se autolimite
*   No trate de aplicar mucha lógica
*   No trate de racionalizar o justificar
*   No dude de usted mismo
*   No trate de auto-convencerse. Que cuando era niño fulanito dijo que no podía, así que voy a pasar mi vida tratando de probar lo contrario.

Para evitar que su razón restrinja su intuición, tómese sólo 3 minutos para contestar cada una de las preguntas. 


#### La primera pregunta es ¿Qué experiencias quiero en mi vida?



*   Piensen en ¿Cómo quiero sentirme conmigo mismo? Desde que me levanto hasta cuando me duermo.
*   Las experiencias también abarcan las personas alrededor mío: mi familia, mis amistades, mis compañeros de trabajo. ¿Qué clase de relaciones quiero mantener? ¿Con qué clase de personas quiero estar? 
*   Las experiencias también pueden ser las actividades que realizo: Explorar lugares nuevos,  crear obras, etc..
*   Las experiencias también pueden incluir el entorno donde vivo. Quiero vivir cerca de la naturaleza, o en medio de la ciudad ambiente metropolitana.

Nosotros los humanos necesitamos sentir cariño, anhelamos sentir asombro, gratitud, satisfacción de haber realizado algo nuevo.

Por eso es que los sentimientos intensos son los que más quedan en nuestros recuerdos.


> Mientras escribe, hágase una autoevaluación de sus respuestas, para evitar caer en conformismo.
> La siguiente frase resume muy bien el criterio para una buena meta.  
> "Una buena meta debe asustarte un poco y emocionarte mucho." - Joe Vitale


Haga una pausa al video, tómese 3 minutos y sigamos.


#### La segunda pregunta es ¿Cómo quiero crecer en mi vida?

Piensen en las diferentes áreas para la mejora: su salud, su conocimiento e intelecto, su emoción, su espíritu.



*   Piensen en ¿Qué parte de mi quiero mejorar?
*   Una vez identificada esas áreas de mejora, para crecer, ¿que debo hacer?, ¿qué clases debo tomar?, ¿que debo practicar?, ¿a quienes debo acudir?
*   ¿Qué retos quiero tomar?
*   Uno de los conocimientos importantes es saber cómo aprender.

Cuando uno deja de crecer, su espíritu se constriñe. Si una langosta se aferra a su caparazón, no crece. Asimismo, si nos aferramos a viejos ideales, no creceremos. Al no crecer, no tendremos nada nuevo que ofrecer y nos sentiremos postrados.

Haga una pausa al video, tómese 3 minutos y sigamos.


#### La tercera pregunta es ¿Cómo quiero contribuir al mundo?

Proyéctese a usted en el futuro, cuando ha acumulado las diferentes experiencias que ha puesto en la lista, y ha crecido en la persona que deseaba. El próximo nivel es trascender, ir más allá de uno mismo, compartir, contribuir. 

La contribución puede con su familia, con el lugar donde trabaja, con la comunidad, con su país, con el planeta. 

Y la contribución puede manifestarse en forma de trabajos voluntarios, trabajos ad honorem, donaciones, iniciativas de beneficio colectivo. También son formas de contribución las actividades creativas que producen positivismo o inspiran  personas como obras de arte.

“El más grande entre vosotros será el que se ponga al servicio de los demás.” 

Mateo 23:11

Nuevamente, haga una pausa al video, tómese 3 minutos para responder y sigamos.


### El segundo ejercicio: preguntas centrales

Ahora que tiene su respuestas a las tres preguntas guías, examínelas e identifique el tema que más prevalece.

Ellas le ayudarán a responder las preguntas centrales:

¿Cuál es el **propósito de su vida** y Cuales son **sus valores fundamentales**? 

Hágase una introspección pensando en ¿Qué es lo que _tanto_ le importa? ¿Que trae significado a mi vida?

Su propósito es esa incesante fuente de motivación, lo que le hace levantarse en la mañana y seguir adelante aun cuando el día está nublado.

Para algunos puede ser la familia, para otros servir a Dios y al prójimo, conservación de la naturaleza, formar empresa y crear trabajos, o crear obras de arte que inspira y deleita a muchos otros.

Tómese su tiempo para articular el propósito de su vida y sus valores fundamentales.


## Vida de éxito

El éxito es cuando está recorriendo una vida alineada con sus propósitos y sus valores.

En ese momento, usted no necesita comparar con nadie, las dificultades son retos alcanzables, los peligros son oportunidades, el sudor es una bendición y el presente es un regalo.

Examine sus respuestas con regularidad, refínelas a medida que usted vaya progresando. Priorice sus actividades para aumentar valores que usted considera fundamentales. No deje que pase un día sin que usted haya hecho algo que contribuya a su propósito. Comprométase hoy, ahora. Solamente con acciones realizaremos nuestros propósitos.

Recuérdese, usted tiene una sola vida, y esa vida merece ser una obra maestra.

Empecemos el 2021 con propósitos y acciones!

> El primer ejercicio está basado en el seminario de Vishen Lakhiani de Mind. El segundo ejercicio está inspirado por los escritos de Viktor Frankl, las teorías de Growth Mindset de Carol Dwek, y conceptos de la agilidad mental de Susan David.