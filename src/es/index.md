---
title: Empodereemos Más - Inicio
layout: layouts/page_with_posts.njk

disableComments: true

pagination:
  data: collections.posts_es
  size: 10
  reverse: true
  alias: posts

description: Empoderemos Mas, sitio de crecimiento personal, desarrollo profesional y mejoramiento social
keywords: ["crecimiento personal", "desarrollo", "profesional", "mejoramiento"]
---

<div 
    style="background-image:
           url('/images/em-background.jpg'); 
    height:200px;
    background-size: 100%; 
    background-position:center;">&nbsp;</div>

# Empoderarse no es una opcion. ¡Es una responsabilidad!

Tantos retos en frente nuestro. ¿Que esperas para empoderarte?

Crecimiento personal, desarrollo profecional y mejoramiento social.